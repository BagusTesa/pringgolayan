<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Pringgolayan | 403</title>
    <meta name="keywords" content="403,No Access,Error,Login Required">
    <!-- Bootstrap -->
    <link href="{{ asset('vendor/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/common.css') }}" rel="stylesheet" >
    <link href="{{ asset('assets/img/favicon.png') }}" rel="shortcut icon">
  </head>
  <body>
    <div class="container center-block text-center">
      <div class="col-md-offset-4 col-md-4">
        <div class="row">
            <a href="{{ route('root') }}"><img src="{{ asset('assets/img/logo403.png') }}" alt="403" class="img-responsive"/></a>
        </div>
      </div>
      <div class="col-md-offset-1 col-md-10">
        <div class="row">
          <h3>
            Sorry, you have <b>no access</b> to this page.
          </h3>
            ...but, you still can access other page...<br><small>maybe</small>
        </div>
      </div>
    </div>
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  </body>
</html>
