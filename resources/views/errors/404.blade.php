<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Pringgolayan | 404</title>
    <meta name="keywords" content="404,No Page,Error,Not Found,Null">
    <!-- Bootstrap -->
    <link href="{{ asset('vendor/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/common.css') }}" rel="stylesheet" >
    <link href="{{ asset('assets/img/favicon.png') }}" rel="shortcut icon">
  </head>
  <body>
    <div class="container center-block text-center">
      <div class="col-md-offset-4 col-md-4">
        <div class="row">
            <a href="{{ route('root') }}"><img src="{{ asset('assets/img/logo404.png') }}" alt="404" class="img-responsive"/></a>
        </div>
      </div>
      <div class="col-md-offset-1 col-md-10">
        <div class="row">
          <h3>
            Sorry the URL you entered <strong>no longer exist</strong> or <b>it</b> <i>doesn't exist</i> until you call upon it.
          </h3>
          <small>
            ...or something may happened to the URL...
          </small>
        </div>
      </div>
    </div>
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  </body>
</html>
