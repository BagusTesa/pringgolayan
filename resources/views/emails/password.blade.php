<!DOCTYPE html>
<html lang="id">
  <head>
    <title>Reset Password Admin Pringgolayan</title>
  </head>
  <body>
    <h1>Reset Password Administrator Web Dusun Pringgolayan</h1>
    <p>
      URL reset password: <a href="{{ route('password.reset.token', $token) }}" target="_blank">{{ route('password.reset.token', $token) }}</a>
    </p>
    <p>
      Dengan mengakses Uniform Resource Locator di atas, saya menyatakan bahwa saya adalah pemilik sah akun e-mail ini dan memang benar-benar pemilik
      akun di website Dusun Pringgolayan. Apabila terbukti saya bukan administrator sah dari website, pemilik website yang terdaftar di PANDI berhak
      menghapus akun saya secara keseluruhan di website Dusun Pringgolayan.
    </p>
    <br>
    <br>
    <i>
      Please <strong>DO NOT Send Email and/or Reply</strong> to this e-mail. Thank You.
    </i>
  </body>
</html>
