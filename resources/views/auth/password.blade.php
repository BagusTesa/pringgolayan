@extends('admin.layouts.master')
@section('title') Pendaftaran Reset Password @endsection
@section('meta') @endsection
@section('css') @endsection
@section('js') @endsection
@section('contents')

<!-- For Response -->
@include('common.layouts.form-warning')

<form class="form-horizontal" action="{{ route('admin.password.reset.post') }}" method="post">
  {!! csrf_field() !!}
  <div class="form-group">
    <label for="email" class="control-label col-md-2">E-mail</label>
    <div class="col-md-10">
      <input type="email" name="email" class="form-control" id="email">
    </div>
  </div>
  <div class="text-right">
    <input type="submit" value="Kirimkan E-mail Reset Password" class="btn btn-success">
  </div>
</form>
@endsection
@section('pagination') @endsection
