@extends('user.layouts.master')
@section('title') Reset Password @endsection
@section('meta') @endsection
@section('css')
  <link href="{{ asset('assets/css/beranda.css') }}" rel="stylesheet">
@endsection
@section('js')
  <script src="{{ asset('assets/js/beranda.js') }}"></script>
@endsection
@section('carousel') @endsection
@section('contents')
<h2>Reset Password</h2>
<hr>

<!-- For Response -->
@include('common.layouts.form-warning')

<form class="form-horizontal" action="{{ route('password.reset.post') }}" method="post">
  {!! csrf_field() !!}
  <input type="hidden" name="token" value="{{ $token }}">
  <div class="form-group">
    <label for="email" class="control-label col-md-2">E-mail</label>
    <div class="col-md-10">
      <input type="email" name="email" class="form-control" id="email">
    </div>
  </div>
  <div class="form-group">
    <label for="password" class="control-label col-md-2">Password</label>
    <div class="col-md-10">
      <input type="password" name="password" class="form-control" id="password">
    </div>
  </div>
  <div class="form-group">
    <label for="password_confirmation" class="control-label col-md-2">Konfirmasi Password</label>
    <div class="col-md-10">
      <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">
    </div>
  </div>
  <div class="col-md-10 col-md-offset-2 text-justify">
    <p class="fa fa-lock">
      Dengan memanfaatkan fitur ini, saya menyatakan dengan benar bahwa akun dengan e-mail ini adalah benar-benar milik saya
      dan saya akan bertanggung jawab sepenuhnya untuk melindungi keamanan akun saya dan nama baik Dusun Pringgolayan
      dalam manajemen website Dusun Pringgolayan sehingga saya tidak memerlukan fitur ini di kemudian hari.
    </p>
  </div>
  <div class="text-right">
    <input type="submit" value="Perbarui" class="btn btn-success">
  </div>
</form>
@endsection
