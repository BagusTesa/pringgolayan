<{{ '?xml' }} version="1.0" encoding="UTF-8" {{ '?' }}>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
  @if($URIs)
  @foreach($URIs as $URI)
  <url>
    <loc>{{ $URI->url }}</loc>
    @if($URI->lastmod)
    <lastmod>{{ $URI->lastmod }}</lastmod>
    @endif
    @if($URI->changefreq)
    <changefreq>{{ $URI->changefreq }}</changefreq>
    @endif
  </url>
  @endforeach
  @endif
</urlset>
