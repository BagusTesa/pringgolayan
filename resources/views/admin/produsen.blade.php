@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') @if($isEdit && isset($Producer->id)) {{ $Producer->name }} @else Daftarkan Produsen Baru @endif @endsection
@section('css') @endsection
@section('js')
  <script src="{{ asset('assets/js/produk.js') }}"></script>
  @include('admin.layouts.tinymce-script')
  @include('admin.layouts.image-refresh')
  @include('admin.layouts.product-refresh')
@endsection
@section('contents')

<!-- Flash Data -->
@include('common.layouts.form-alerter')
@include('common.layouts.form-success')

<form class="form-horizontal" action="@if($isEdit && ($Producer->id != null)){{ route('admin.producer.update', $Producer->id) }}@else{{ route('admin.producer.store') }}@endif" method="post">
  {!! csrf_field() !!}
  @if($isEdit && ($Producer->id != null))
  {!! method_field('put') !!}
  @endif
  <div class="form-group">
    <label for="name" class="col-md-2 control-label">Nama</label>
    <div class="col-md-10">
      <input type="text" name="name" value="@if($isEdit){{ $Producer->name }}@endif" class="form-control" id="name" required placeholder="Nama penduduk pringgolayan">
    </div>
  </div>
  <div class="form-group">
    <label for="address" class="col-md-2 control-label">Alamat</label>
    <div class="col-md-10">
      <input type="text" name="address" value="@if($isEdit){{ $Producer->address }}@endif" class="form-control" id="address" placeholder="Alamat lengkap">
    </div>
  </div>
  <div class="form-group">
    <label for="contact" class="col-md-2 control-label">Kontak</label>
    <div class="col-md-10">
      <input type="text" name="contact" value="@if($isEdit){{ $Producer->contact }}@endif" class="form-control" id="contact" placeholder="Kontak yang dapat dihubungi, contoh: nomor HP, e-mail, dsb">
    </div>
  </div>
  <div class="form-group">
    <label for="image" class="col-md-2 control-label">Foto</label>
    <div class="col-md-10">
      <select class="form-control" name="image" id="image">
        <option value="-1"> - </option>
        @foreach($Images as $Image)
        <option value="{{ $Image->id }}" @if($isEdit) @if($Image->id == $Producer->image_id){{ 'selected' }}@endif @endif>{{ $Image->name }}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <textarea class="frontier" id="documentEditor" name="description">@if($isEdit){{ $Producer->description }}@endif</textarea>
    </div>
  </div>
  @if($isEdit && ($Producer->id != null))
  @foreach($Producer->Product as $sProduct)
  <div class="form-group duplicable">
    <label for="newProduct" class="control-label col-md-2">Produk</label>
    <div class="col-md-10">
      <div class="row">
        <div class="col-xs-10">
          <select name="product[]" class="form-control" id="newProduct">
            <option value="-1">-</option>
            @foreach($Products as $Product)
            <option value="{{ $Product->id }}" @if($Product->id == $sProduct->id){{ 'selected' }}@endif>{{ $Product->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-xs-2">
          <button class="removeField btn btn-danger"><i class="fa fa-close"></i></button>
        </div>
      </div>
    </div>
  </div>
  @endforeach
  <div class="form-group duplicable">
    <label for="newProduct" class="control-label col-md-2">Produk</label>
    <div class="col-md-10">
      <div class="row">
        <div class="col-xs-10">
          <select name="product[]" class="form-control" id="newProduct">
            <option value="-1">-</option>
            @foreach($Products as $Product)
            <option value="{{ $Product->id }}">{{ $Product->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-xs-2">
          <button class="removeField btn btn-danger"><i class="fa fa-close"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row fieldAdder">
    <div class="col-md-12">
      <button href="#" class="addField btn btn-primary">Tambah Produk</button>
    </div>
  </div>
  @else
  <div class="alert alert-success">
    Produk yang akan direlasikan pada produsen dapat ditambahkan setelah data produsen disimpan.
  </div>
  @endif
  <div class="form-group text-right">
    <div class="col-md-12">
      @if($isEdit && $Producer->id != null)<button type="button" name="RefreshAndReload" class="btn btn-default"><i class="fa fa-refresh"></i> Muat Ulang Relasi</button>@endif
      <input type="submit" value="@if($isEdit){{ 'Simpan' }}@else{{ 'Tambahkan' }}@endif" class="btn btn-success">
    </div>
  </div>
</form>
<div class="hidden-xs hidden-sm hidden-md hidden-lg">
  <!-- Last Order -->
  <div class="form-group duplicable">
    <label for="newProduct" class="control-label col-md-2">Produk</label>
    <div class="col-md-10">
      <div class="row">
        <div class="col-xs-10">
          <select name="product[]" class="form-control" id="newProduct">
            <option value="-1">-</option>
            @foreach($Products as $Product)
            <option value="{{ $Product->id }}">{{ $Product->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-xs-2">
          <button class="removeField btn btn-danger"><i class="fa fa-close"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('pagination') @endsection
