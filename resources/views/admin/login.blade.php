<!DOCTYPE html>
<html lang="id">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title> Pringgolayan (Administrasi) | Login </title>
    <!-- Bootstrap -->
    <link href="{{ asset('vendor/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Timeline CSS -->
    <link href="{{ asset('vendor/startbootstrap-sb-admin-2/dist/css/timeline.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('vendor/startbootstrap-sb-admin-2/dist/css/sb-admin-2.css') }}" rel="stylesheet">

    <!-- Common CSS -->
    <link href="{{ asset('assets/css/common.css') }}" rel="stylesheet" >
    <link href="{{ asset('assets/img/favicon.png') }}" rel="shortcut icon">

    <link href="{{ asset('assets/css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  </head>
  <body>
    <!-- Content Begin -->
    <div class="container" style="margin-top: 20vh;margin-bottom: 20vh;">
      <div class="row">
        <div class="col-md-offset-4 col-md-4">
          <div class="row">
            <a href="{{ route('root') }}"><img src="{{ asset('assets/img/logo2.png') }}" alt="Pringgolayan" class="img-responsive"/></a>
          </div>

          <!-- Flash Data -->
          @include('common.layouts.form-alerter')
          @include('common.layouts.form-success')

          <form class="form-horizontal" action="{{ route('admin.login') }}" method="post">
            <div class="form-group">
              <label class="col-md-4 control-label" for="username">User Name:</label>
              <div class="col-md-8">
                <input type="text" name="username" class="form-control" id="username">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="password">Password:</label>
              <div class="col-md-8">
                <input type="password" name="password" class="form-control" id="password">
              </div>
            </div>
            {!! csrf_field() !!}
            <input type="submit" value="Login" class="btn btn-success pull-right">
          </form>
        </div>
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- Custom Theme JavaScript -->
  </body>
</html>
