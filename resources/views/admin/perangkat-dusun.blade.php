@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') Perangkat Dusun @endsection
@section('css') @endsection
@section('js')
  @include('admin.layouts.image-refresh')
@endsection
@section('contents')

<!-- Flash Data -->
@include('common.layouts.form-alerter')
@include('common.layouts.form-success')

<div>
  <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#tambahPerangkatDusun">Tambah Perangkat Dusun</a>
</div>
<hr>

<div class="modal fade" id="tambahPerangkatDusun" tabindex="-1" role="dialog" aria-labelledby="tambahPerangkatDusunLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="tambahGambarLabel">Tambah Perangkat Dusun</h4>
      </div>
      <div class="modal-body">
        <form class="" action="{{ route('admin.authorities.store') }}" method="post">
          {!! csrf_field() !!}
          <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="name" id="nama" placeholder="Nama" required>
          </div>
          <div class="form-group">
            <label for="posisi">Posisi</label>
            <input type="text" class="form-control" name="position" id="posisi" placeholder="Posisi (mis. Kepala Dukuh)" required>
          </div>
          <div class="form-group">
            <label for="gambar">Foto</label>
            <select class="form-control" name="image">
              <option value="-1"> - </option>
              @foreach($Images as $Image)
              <option value="{{ $Image->id }}">{{ $Image->name }}</option>
              @endforeach
            </select>
          </div>
          <hr>
          <div class="text-right">
            <input type="submit" value="Simpan" class="btn btn-success">
            <input type="reset" value="Batal" class="btn btn-danger" data-dismiss="modal">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<table class="table table-hover">
  <colgroup>
    <col span="1" style="width: 5%;"></col>
    <col span="1" style="width: 35%;"></col>
    <col span="1" style="width: 30%"></col>
    <col span="1" style="width: 10%"></col>
    <col span="1" style="width: 10%;"></col>
    <col span="1" style="width: 5%;"></col>
    <col span="1" style="width: 5%;"></col>
  </colgroup>
  <tr>
    <th>No</th>
    <th>Nama</th>
    <th>Posisi</th>
    <th>Urutan</th>
    <th>Gambar</th>
    <th></th>
    <th></th>
  </tr>
  <?php $iCounter = 1 ?>
  @foreach($Authorities as $Authority)
  <tr>
    <td>{{ $iCounter++ }}</td>
    <td>{{ $Authority->name }}</td>
    <td>{{ $Authority->position }}</td>
    <td>{{ $Authority->order }}</td>
    <td>@if($Authority->Image != null){{ $Authority->Image->name }}@else{{ '-' }}@endif</td>
    <td><a href="#" class="btn btn-default" data-toggle="modal" data-target="#suntingPerangkatDusun{{ $Authority->id }}">Sunting</a></td>
    <td><a href="#" class="btn btn-danger" data-toggle="modal" data-target="#hapusPerangkatDusun{{ $Authority->id }}">Hapus</a></td>
  </tr>
  @endforeach
</table>

@foreach($Authorities as $Authority)
<div class="modal fade" id="suntingPerangkatDusun{{ $Authority->id }}" tabindex="-1" role="dialog" aria-labelledby="suntingPerangkatDusunLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="tambahGambarLabel">Sunting Perangkat Dusun</h4>
      </div>
      <div class="modal-body">
        <form class="" action="{{ route('admin.authorities.update', $Authority->id) }}" method="post">
          {!! csrf_field() !!}
          {!! method_field('put') !!}
          <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="name" id="nama" placeholder="Nama" value="{{ $Authority->name }}" required>
          </div>
          <div class="form-group">
            <label for="posisi">Posisi</label>
            <input type="text" class="form-control" name="position" id="posisi" placeholder="Posisi (mis. Kepala Dukuh)" value="{{ $Authority->position }}" required>
          </div>
          <div class="form-group">
            <label for="urutan">Urutan</label>
            <input type="text" class="form-control" name="order" id="order" placeholder="Untuk pengurutan penampilan pada halaman tentang dusun)" value="{{ $Authority->order }}">
          </div>
          <div class="form-group">
            <label for="gambar">Foto</label>
            <select class="form-control" name="image">
              <option value="-1"> - </option>
              @foreach($Images as $Image)
              <option value="{{ $Image->id }}" @if(isset($Authority->Image) && ($Authority->Image->id == $Image->id)){{ 'selected' }}@endif>{{ $Image->name }}</option>
              @endforeach
            </select>
          </div>
          <hr>
          <div class="text-right">
            <input type="submit" value="Simpan" class="btn btn-success">
            <input type="reset" value="Batal" class="btn btn-danger" data-dismiss="modal">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="hapusPerangkatDusun{{ $Authority->id }}" tabindex="-1" role="dialog" aria-labelledby="hapusPerangkatDusunLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="tambahGambarLabel">Hapus Perangkat Dusun</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin menghapus <strong>{{$Authority->name}}</strong> ({{ $Authority->position }}) dari daftar perangkat dusun?</p>
      </div>
      <div class="modal-footer">
        <form action="{{ route('admin.authorities.destroy', $Authority->id) }}" method="post">
          {!! csrf_field() !!}
          {!! method_field('delete') !!}
          <input type="submit" value="Hapus" class="btn btn-danger">
          <input type="reset"  value="Batal" class="btn btn-default" data-dismiss="modal">
        </form>
      </div>
    </div>
  </div>
</div>
@endforeach

@endsection
@section('pagination') @endsection
