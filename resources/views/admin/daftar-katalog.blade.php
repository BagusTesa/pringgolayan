@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') Katalog @endsection
@section('css') @endsection
@section('js') @endsection
@section('contents')

<?php $searchURL = route('admin.catalog.search'); ?>
@include('admin.layouts.search')

<!-- Flash Data -->
@include('common.layouts.form-alerter')
@include('common.layouts.form-success')

<div class="table-responsive">
  <table class="table table-hover">
    <colgroup>
      <col span="1" style="width: 5%;"></col>
      <col span="1" style="width: 45%;"></col>
      <col span="1" style="width: 40%"></col>
      <col span="1" style="width: 5%;"></col>
      <col span="1" style="width: 5%;"></col>
    </colgroup>
    <tr>
      <th>No</th>
      <th>Katalog</th>
      <th>Jumlah Produk</th>
      <th></th>
      <th></th>
    </tr>
    <?php $iCounter = 1 ?>
    @foreach($Catalogs as $Catalog)
    <tr>
      <td>{{ $iCounter++ }}</td>
      <td><a href="{{ route('admin.catalog.show', $Catalog->id) }}">{{ $Catalog->name }}</a></td>
      <td>{{ count($Catalog->Product) }}</td>
      <td><a class="btn btn-default" href="{{ route('admin.catalog.edit', $Catalog->id) }}">Sunting</a></td>
      <td><a class="btn btn-danger" data-toggle="modal" data-target="#confirmation{{ $Catalog->id }}">Hapus</a></td>
    </tr>
    @endforeach
  </table>
</div>
<!-- Modal -->
@foreach($Catalogs as $Catalog)
<div class="modal fade" id="confirmation{{ $Catalog->id }}" tabindex="-1" role="dialog" aria-labelledby="confirmationLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="confirmationLabel">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin menghapus katalog <strong>{{$Catalog->name}}</strong>?</p>
        <p>Sejumlah {{ count($Catalog->Product) }} produk juga akan <strong>terhapus secara permanen</strong>!</p>
      </div>
      <div class="modal-footer">
        <form action="{{ route('admin.catalog.destroy', $Catalog->id) }}" method="post">
          {!! csrf_field() !!}
          {!! method_field('delete') !!}
          <input type="submit" value="Hapus" class="btn btn-danger">
          <input type="reset"  value="Batal" class="btn btn-default" data-dismiss="modal">
        </form>
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection
@section('pagination') {!! $Catalogs->render() !!} @endsection
