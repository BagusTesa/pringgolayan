@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') Produk @endsection
@section('css') @endsection
@section('js') @endsection
@section('contents')

<?php $searchURL = route('admin.product.search'); ?>
@include('admin.layouts.search')

<!-- Flash Data -->
@include('common.layouts.form-alerter')
@include('common.layouts.form-success')

<div class="table-responsive">
  <table class="table table-hover">
    <colgroup>
      <col span="1" style="width: 5%;"></col>
      <col span="1" style="width: 45%;"></col>
      <col span="1" style="width: 40%"></col>
      <col span="1" style="width: 5%;"></col>
      <col span="1" style="width: 5%;"></col>
    </colgroup>
    <tr>
      <th>No</th>
      <th>Nama Produk</th>
      <th>Katalog</th>
      <th></th>
      <th></th>
    </tr>
    <?php $iCounter = 1 ?>
    @foreach($Products as $Product)
    <tr>
      <td>{{ $iCounter++ }}</td>
      <td><a href="{{ route('catalog.product.show', [$Product->Catalog->slug, $Product->slug]) }}" target="_blank">{{ $Product->name }}</a></td>
      <td><a href="{{ route('admin.catalog.show', $Product->Catalog->id) }}">{{ $Product->Catalog->name }}</a></td>
      <td><a class="btn btn-default" href="{{ route('admin.product.edit', $Product->id) }}">Sunting</a></td>
      <td><a class="btn btn-danger" data-toggle="modal" data-target="#confirmation{{ $Product->id }}">Hapus</a></td>
    </tr>
    @endforeach
  </table>
</div>
<!-- Modal -->
@foreach($Products as $Product)
<div class="modal fade" id="confirmation{{ $Product->id }}" tabindex="-1" role="dialog" aria-labelledby="confirmationLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="confirmationLabel">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin menghapus produk <strong>{{$Product->name}}</strong>?</p>
      </div>
      <div class="modal-footer">
        <form action="{{ route('admin.product.destroy', $Product->id) }}" method="post">
          {!! csrf_field() !!}
          {!! method_field('delete') !!}
          <input type="submit" value="Hapus" class="btn btn-danger">
          <input type="reset"  value="Batal" class="btn btn-default" data-dismiss="modal">
        </form>
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection
@section('pagination') {!! $Products->render() !!} @endsection
