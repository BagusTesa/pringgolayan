@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') Demografik Dusun @endsection
@section('css') @endsection
@section('js')
  @include('admin.layouts.image-refresh')
@endsection
@section('contents')

<!-- Flash Data -->
@include('common.layouts.form-alerter')
@include('common.layouts.form-success')

<div>
  <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#tambahDemografik">Tambah Kategori Demografik</a>
</div>
<hr>

<div class="modal fade" id="tambahDemografik" tabindex="-1" role="dialog" aria-labelledby="tambahPerangkatDusunLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="tambahGambarLabel">Tambah Kategori Demografi</h4>
      </div>
      <div class="modal-body">
        <form class="" action="{{ route('admin.demographics.store') }}" method="post">
          {!! csrf_field() !!}
          <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="name" id="nama" placeholder="Nama" required>
          </div>
          <div class="form-group">
            <label for="jumlah">Jumlah</label>
            <input type="text" class="form-control" name="number" id="jumlah" placeholder="Jumlah warga yang memiliki mata pencaharian ini" required>
          </div>
          <div class="form-group">
            <label for="gambar">Foto Representasi</label>
            <select class="form-control" name="image">
              <option value="-1"> - </option>
              @foreach($Images as $Image)
              <option value="{{ $Image->id }}">{{ $Image->name }}</option>
              @endforeach
            </select>
          </div>
          <hr>
          <div class="text-right">
            <input type="submit" value="Simpan" class="btn btn-success">
            <input type="reset" value="Batal" class="btn btn-danger" data-dismiss="modal">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<table class="table table-hover">
  <colgroup>
    <col span="1" style="width: 5%;"></col>
    <col span="1" style="width: 40%;"></col>
    <col span="1" style="width: 35%"></col>
    <col span="1" style="width: 10%;"></col>
    <col span="1" style="width: 10%;"></col>
  </colgroup>
  <tr>
    <th>No</th>
    <th>Nama</th>
    <th>Jumlah</th>
    <th></th>
    <th></th>
  </tr>
  <?php $iCounter = 1 ?>
  @foreach($Demographics as $Demographic)
  <tr>
    <td>{{ $iCounter++ }}</td>
    <td>{{ $Demographic->name }}</td>
    <td>{{ $Demographic->number }}</td>
    <td><a href="#" class="btn btn-default" data-toggle="modal" data-target="#suntingDemografik{{ $Demographic->id }}">Sunting</a></td>
    <td><a href="#" class="btn btn-danger" data-toggle="modal" data-target="#hapusDemografik{{ $Demographic->id }}">Hapus</a></td>
  </tr>
  @endforeach
</table>

@foreach($Demographics as $Demographic)
<div class="modal fade" id="suntingDemografik{{ $Demographic->id }}" tabindex="-1" role="dialog" aria-labelledby="suntingDemografikLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="tambahGambarLabel">Sunting Data Demografik</h4>
      </div>
      <div class="modal-body">
        <form class="" action="{{ route('admin.demographics.update', $Demographic->id) }}" method="post">
          {!! csrf_field() !!}
          {!! method_field('put') !!}
          <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="name" id="nama" placeholder="Nama" value="{{ $Demographic->name }}" required>
          </div>
          <div class="form-group">
            <label for="jumlah">Jumlah</label>
            <input type="text" class="form-control" name="number" id="jumlah" placeholder="Jumlah warga yang memiliki mata pencaharian ini" value="{{ $Demographic->number }}" required>
          </div>
          <div class="form-group">
            <label for="gambar">Foto Representasi</label>
            <select class="form-control" name="image">
              <option value="-1"> - </option>
              @foreach($Images as $Image)
              <option value="{{ $Image->id }}" @if( isset($Demographic->Image) && ($Demographic->Image->id == $Image->id)){{ 'selected' }}@endif>{{ $Image->name }}</option>
              @endforeach
            </select>
          </div>
          <hr>
          <div class="text-right">
            <input type="submit" value="Simpan" class="btn btn-success">
            <input type="reset" value="Batal" class="btn btn-danger" data-dismiss="modal">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="hapusDemografik{{ $Demographic->id }}" tabindex="-1" role="dialog" aria-labelledby="hapusDemografikLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="tambahGambarLabel">Hapus Data Demografik</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin menghapus <strong>{{$Demographic->name}}</strong>?</p>
      </div>
      <div class="modal-footer">
        <form action="{{ route('admin.demographics.destroy', $Demographic->id) }}" method="post">
          {!! csrf_field() !!}
          {!! method_field('delete') !!}
          <input type="submit" value="Hapus" class="btn btn-danger">
          <input type="reset"  value="Batal" class="btn btn-default" data-dismiss="modal">
        </form>
      </div>
    </div>
  </div>
</div>
@endforeach

@endsection
@section('pagination') {!! $Demographics->render() !!} @endsection
