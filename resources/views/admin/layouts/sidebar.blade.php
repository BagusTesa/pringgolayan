<!-- Sidebar -->
<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="#"><i class="fa fa-pencil-square-o fa-fw"></i> Artikel<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
          <li>
            <a href="{{ route('admin.category.index') }}">Daftar Kategori</a>
          </li>
          <li>
            <a href="{{ route('admin.category.create') }}">Buat Kategori Baru</a>
          </li>
          <hr class="sidebar-separator">
          <li>
            <a href="{{ route('admin.article.index') }}">Daftar Semua Artikel</a>
          </li>
          <li>
            <a href="{{ route('admin.article.create') }}">Buat Artikel Baru</a>
          </li>
        </ul>
        <!-- /.nav-second-level -->
      </li>
      <li>
        <a href="#"><i class="fa fa-tag fa-fw"></i> Produk<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
          <li>
            <a href="{{ route('admin.catalog.index') }}">Daftar Katalog</a>
          </li>
          <li>
            <a href="{{ route('admin.catalog.create') }}">Buat Katalog Baru</a>
          </li>
          <hr class="sidebar-separator">
          <li>
            <a href="{{ route('admin.product.index') }}">Daftar Produk</a>
          </li>
          <li>
            <a href="{{ route('admin.product.create') }}">Tambah Produk Baru</a>
          </li>
          <hr class="sidebar-separator">
          <li>
            <a href="{{ route('admin.producer.index') }}">Data Produsen</a>
          </li>
          <li>
            <a href="{{ route('admin.producer.create') }}">Tambah Produsen</a>
          </li>
        </ul>
        <!-- /.nav-second-level -->
      </li>
      <li>
        <a href="#"><i class="fa fa-play-circle-o fa-fw"></i> Manajemen Multimedia<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
          <li>
            <a href="{{ route('admin.image.index') }}">Manajer Citra</a>
          </li>
        </ul>
      </li>
      <li>
        <a href="#"><i class="fa fa-users fa-fw"></i> Pengguna<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
          <li>
            <a href="{{ route('admin.user.index') }}">Daftar Pengguna</a>
          </li>
          <li>
            <a href="{{ route('admin.user.create') }}">Daftarkan Pengguna Baru</a>
          </li>
          <li>
            <a href="{{ route('admin.password.reset') }}">Reset Password Pengguna</a>
          </li>
        </ul>
        <!-- /.nav-second-level -->
      </li>
      <li>
        <a href="#"><i class="fa fa-flag fa-fw"></i> Tentang Pringgolayan<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
          <li>
            <a href="{{ route('admin.authorities.index') }}">Data Pengurus Dusun</a>
          </li>
          <li>
            <a href="{{ route('admin.demographics.index') }}">Data Demografi</a>
          </li>
        </ul>
        <!-- /.nav-second-level -->
      </li>
    </ul>
  </div>
  <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
