<script>
  var fImage = function()
  {
    var b = jQuery.ajax({
      url: "{{ route('ajax.images.list') }}",
      type: "get",
      dataType: "json",
    })
    .done(function(msg){
      var str = "<option value=\"-1\"> - </option>\n";
      for(index = 0; index < msg.length; index++)
      {
        str += "<option value=\"" + msg[index].id + "\">" + msg[index].title + "</option>\n";
      }
      var selected = jQuery("[name='image'], [name='image[]'], [name='intro_image']");
      selected.each(function(){
        var oldValue = $(this).val();
        $(this).html(str).children("[value=" + oldValue + "]").attr("selected", "");
      });
    });
  }
  jQuery("[name='RefreshAndReload']").click(fImage);
  setInterval(fImage, 30000);
</script>
{{-- <button type="button" name="RefreshAndReload" class="btn btn-default"><i class="fa fa-refresh"></i> Muat Ulang Relasi</button> --}}
