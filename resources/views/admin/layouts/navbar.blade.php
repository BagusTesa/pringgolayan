<!-- Navbar -->
<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
  <a class="navbar-brand" href="{{ route('admin.root') }}"><img class="logo img-responsive" src="{{ asset('assets/img/logo2.png') }}" alt="Pringgolayan" /></a>
</div>
<!-- /.navbar-header -->

<ul class="nav navbar-top-links navbar-right">
  <li>
    <a href="{{ route('root') }}" target="_blank"><i class="fa fa-external-link"></i> Lihat Situs</a>
  </li>
  <!-- /.dropdown -->
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
      <i class="fa fa-user fa-fw"></i> {{ $lUser->realname }} <i class="fa fa-caret-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-user">
      <li><a href="{{ route('admin.user.show', $lUser->id) }}"><i class="fa fa-user fa-fw"></i> User Profile</a>
      </li>
      <li><a href="{{ route('admin.user.edit', $lUser->id) }}"><i class="fa fa-gear fa-fw"></i> Settings</a>
      </li>
      <li class="divider"></li>
      <li><a href="{{ route('admin.logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
      </li>
    </ul>
    <!-- /.dropdown-user -->
  </li>
  <!-- /.dropdown -->
</ul>
<!-- /.navbar-top-links -->
