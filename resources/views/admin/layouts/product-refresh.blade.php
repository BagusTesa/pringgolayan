<script>
  var fProduct = function()
  {
    var b = jQuery.ajax({
      url: "{{ route('ajax.product.list') }}",
      type: "get",
      dataType: "json",
    })
    .done(function(msg){
      var str = "<option value=\"-1\"> - </option>\n";
      for(index = 0; index < msg.length; index++)
      {
        str += "<option value=\"" + msg[index].id + "\">" + msg[index].name + "</option>\n";
      }
      var selected = jQuery("[name='product[]']");
      selected.each(function(){
        var oldValue = $(this).val();
        $(this).html(str).children("[value=" + oldValue + "]").attr("selected", "");
      });
    });
  }
  jQuery("[name='RefreshAndReload']").click(fProduct);
  setInterval(fProduct, 30000);
</script>
{{-- <button type="button" name="RefreshAndReload" class="btn btn-default"><i class="fa fa-refresh"></i> Muat Ulang Relasi</button> --}}
