@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') Produsen @endsection
@section('css') @endsection
@section('js') @endsection
@section('contents')

<?php $searchURL = route('admin.producer.search'); ?>
@include('admin.layouts.search')

<!-- Flash Data -->
@include('common.layouts.form-alerter')
@include('common.layouts.form-success')
<table class="table table-hover">
  <colgroup>
    <col span="1" style="width: 5%;"></col>
    <col span="1" style="width: 20%;"></col>
    <col span="1" style="width: 45%"></col>
    <col span="1" style="width: 20%"></col>
    <col span="1" style="width: 5%;"></col>
    <col span="1" style="width: 5%;"></col>
  </colgroup>
  <tr>
    <th>No</th>
    <th>Nama</th>
    <th>Alamat</th>
    <th>Kontak</th>
    <th></th>
    <th></th>
  </tr>
  <?php $iCounter = 1 ?>
  @foreach($Producers as $Producer)
  <tr>
    <td>{{ $iCounter++ }}</td>
    <td><a href="{{ route('producer.show', $Producer->slug) }}" target="_blank">{{ $Producer->name }}</a></td>
    <td>{{ $Producer->address }}</td>
    <td>{{ $Producer->contact }}</td>
    <td><a href="{{ route('admin.producer.edit', [$Producer->id]) }}" class="btn btn-default">Sunting</a></td>
    <td><a href="#" class="btn btn-danger" data-toggle="modal" data-target="#hapusProdusen{{ $Producer->id }}">Hapus</a></td>
  </tr>
  @endforeach
</table>

@foreach($Producers as $Producer)
<div class="modal fade" id="hapusProdusen{{ $Producer->id }}" tabindex="-1" role="dialog" aria-labelledby="hapusProdusenLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="hapusProdusenLabel">Hapus Produsen</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin ingin menghapus <strong>{{$Producer->name}}</strong> ({{ str_limit($Producer->address, 100) }}) dari daftar produsen?</p>
      </div>
      <div class="modal-footer">
        <form action="{{ route('admin.producer.destroy', $Producer->id) }}" method="post">
          {!! csrf_field() !!}
          {!! method_field('delete') !!}
          <input type="submit" value="Hapus" class="btn btn-danger">
          <input type="reset"  value="Batal" class="btn btn-default" data-dismiss="modal">
        </form>
      </div>
    </div>
  </div>
</div>
@endforeach

@endsection
@section('pagination') {!! $Producers->render() !!} @endsection
