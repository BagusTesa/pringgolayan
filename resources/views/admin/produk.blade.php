@extends('admin.layouts.master')
@section('meta') @endsection
@section('title') @if($isEdit) {{ $Product->name }} @else Daftarkan Produk Baru @endif @endsection
@section('css') @endsection
@section('js')
  <script src="{{ asset('assets/js/produk.js') }}"></script>
  @include('admin.layouts.tinymce-script')
  @include('admin.layouts.image-refresh')
@endsection
@section('contents')

<!-- Flash Data -->
@include('common.layouts.form-alerter')
@include('common.layouts.form-success')

<form class="form-horizontal" method="post" action="@if($isEdit && ($Product->id != null)){{ route('admin.product.update', $Product->id) }}@else{{ route('admin.product.store')}}@endif ">
  {!! csrf_field() !!}
  @if($isEdit && ($Product->id != null))
  {!! method_field('put') !!}
  @endif
  <div class="form-group">
    <label class="col-md-2 control-label" for="judul">Judul Artikel</label>
    <div class="col-md-10">
      <input id="judul" class="form-control" type="text" name="name" value="@if($isEdit){{ $Product->name }}@endif" placeholder="Masukkan nama produk" required/>
    </div>
  </div>
  <div class="form-group">
    <label class="col-md-2 control-label" for="catalog">Katalog</label>
    <div class="col-md-10">
      <select class="form-control" id="catalog" name="catalog" required>
        @foreach($Catalogs as $Catalog)
          <option value="{{ $Catalog->id }}" @if($isEdit && ($Catalog->id == $Product->Catalog->id)){{ 'selected' }}@endif>{{ $Catalog->name }}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <textarea class="frontier" id="documentEditor" name="description">@if($isEdit){{ $Product->description }}@endif</textarea>
    </div>
  </div>
  @if($isEdit && ($Product->id != null))
  @foreach($Product->Image as $sImage)
  <div class="form-group duplicable">
    <label for="newImage" class="control-label col-md-2">Gambar</label>
    <div class="col-md-10">
      <div class="row">
        <div class="col-xs-10">
          <select name="image[]" class="form-control">
            <option value="-1"> - </option>
            @foreach($Images as $Image)
            <option value="{{ $Image->id }}" @if($Image->id == $sImage->id){{ 'selected' }}@endif>{{ $Image->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-xs-2">
          <button class="removeField btn btn-danger"><i class="fa fa-close"></i></button>
        </div>
      </div>
    </div>
  </div>
  @endforeach
  <div class="form-group duplicable">
    <label for="newImage" class="control-label col-md-2">Gambar</label>
    <div class="col-md-10">
      <div class="row">
        <div class="col-xs-10">
          <select name="image[]" class="form-control" id="newImage">
            <option value="-1">-</option>
            @foreach($Images as $Image)
            <option value="{{ $Image->id }}">{{ $Image->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-xs-2">
          <button class="removeField btn btn-danger"><i class="fa fa-close"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row fieldAdder">
    <div class="col-md-12">
      <button href="#" class="addField btn btn-primary">Tambah Gambar</button>
    </div>
  </div>
  @else
  <div class="alert alert-success">
    Gambar-gambar produk dapat ditambahkan setelah produk disimpan. Namun anda tetap bisa menambahkan gambar pada editor WYSIWYG yang ada.
  </div>
  @endif
  <hr>
  <div class="form-group text-right">
    <div class="col-md-12">
      @if($isEdit && $Product->id != null)<button type="button" name="RefreshAndReload" class="btn btn-default"><i class="fa fa-refresh"></i> Muat Ulang Relasi</button>@endif
      <input type="submit" class="btn btn-success" value="@if($isEdit){{ 'Simpan' }}@else{{ 'Publikasikan' }}@endif">
    </div>
  </div>
</form>

<div class="hidden-xs hidden-sm hidden-md hidden-lg">
  <!-- last order -->
  <div class="form-group duplicable">
    <label for="newImage" class="control-label col-md-2">Gambar</label>
    <div class="col-md-10">
      <div class="row">
        <div class="col-xs-10">
          <select name="image[]" class="form-control">
            <option value="-1">-</option>
            @foreach($Images as $Image)
            <option value="{{ $Image->id }}">{{ $Image->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-xs-2">
          <button class="removeField btn btn-danger"><i class="fa fa-close"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('pagination') @endsection
