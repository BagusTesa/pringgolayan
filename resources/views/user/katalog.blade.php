@extends('user.layouts.master')
@section('title') Katalog - {{ $Catalog->name }} @endsection
@section('meta')
  <meta name="keyword" content="Pringgolayan,Banguntapan,Kerajinan Kuningan,Kota Gede,Katalog Produk,{{ $Catalog->name }}">
  <meta name="description" content="{{ 'Produk-produk ' . $Catalog->name . ' Website Dusun Pringgolyan, Banguntapan' }}">
  <?php
    $fURL         = route('catalog.show', $Catalog->slug);
    $fType        = 'product.group';
    $fTitle       = 'Katalog - ' . $Catalog->name;
    $fDescription = 'Produk-produk ' . $Catalog->name . ' Website Dusun Pringgolyan, Banguntapan';
    if($Products[0] != null)
    {
      if($Products[0]->Image->first() != null)
      {
        $fImage   = $Products[0]->Image->first()->image_path;
      }
      else
      {
        $fImage   = null;
      }
    }
    else
    {
      $fImage     = null;
    }
  ?>
  @include('user.layouts.facebook-meta')
@endsection
@section('css')
  <link href="{{ asset('assets/css/katalog.css') }}" rel="stylesheet">
@endsection
@section('js') @endsection
@section('contents')
<h2>Produk {{ $Catalog->name }}</h2>
<?php $u = count($Products); ?>
@for($i = 0; $i < $u; $i++)
<div class="row center-block">
  @for($p = $i; ($p < ($i+3)) && ($p < $u); $p++)
  <div class="col-md-3 product-list">
    <div class="row">
      <div class="thumbnail-container">
        <div class="thumbnail">
          <a href="{{ route('catalog.product.show', [$Catalog->slug, $Products[$p]->slug]) }}">
          @if($Products[$p]->Image->first() != null)
          <img src="{{ route('images', [$Products[$p]->Image->first()->image_path]) }}" class="thumbnail-img img-responsive" alt="{{ $Products[$p]->Image->first()->description }}" />
          @else
          <img src="{{ route('images', 'noImage.png') }}" class="thumbnail-img img-responsive" alt="{{ $Products[$p]->name }}" />
          @endif
          </a>
        </div>
      </div>
    </div>
    <div class="row text-center">
      <a href="{{ route('catalog.product.show', [$Catalog->slug, $Products[$p]->slug]) }}">
      <strong>{{ $Products[$p]->name }}</strong>
      </a>
    </div>
    <div class="row text-center">
      {{ str_limit(strip_tags($Products[$p]->description), 45) }}
    </div>
  </div>
  @endfor
  <?php $i += 2; ?>
</div>
@endfor
@if(count($Products) == 0)
<div class="row Post text-center">
  <b>Belum ada produk</b>
</div>
@else
<div class="text-center">
  {!! $Products->render() !!}
</div>
@endif
@endsection
