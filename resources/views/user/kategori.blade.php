@extends('user.layouts.master')
@section('title') Kategori - {{ $Category->name }} @endsection
@section('meta')
  <meta name="keywords" content="Pringgolayan,Banguntapan,Kerajinan Kuningan,Kota Gede,Artikel,{{ $Category->name }}">
  <meta name="description" content="{{ 'Artikel-artikel ' . $Category->name . ' Website Dusun Pringgolyan, Banguntapan' }}">
  <?php
    $fURL         = route('category.show', $Category->slug);
    $fType        = 'article';
    $fTitle       = 'Kategori - ' . $Category->name;
    $fDescription = 'Artikel-artikel ' . $Category->name . ' Website Dusun Pringgolyan, Banguntapan';
    if($Posts[0] != null)
    {
      if($Posts[0]->Image != null)
      {
        $fImage   = $Posts[0]->Image->image_path;
      }
      else
      {
        $fImage   = null;
      }
    }
    else
    {
      $fImage     = null;
    }
  ?>
  @include('user.layouts.facebook-meta')
@endsection
@section('css')
  <link href="{{ asset('assets/css/beranda.css') }}" rel="stylesheet">
@endsection
@section('js') @endsection
@section('contents')
<h2>{{ $Category->name }}</h2>
<hr>
@foreach($Posts as $Post)
<div class="row Post">
  <div class="col-md-4">
    <a href="{{ route('category.article.show', [$Category->slug, $Post->slug]) }}">
    @if($Post->intro_image != null)
    <img class="img-responsive center-block" src="{{ route('images', $Post->Image->image_path) }}" alt="{{ $Post->Image->description }}" />
    @else
    <img class="img-responsive center-block" src="{{ route('images', 'noImage.png') }}" alt="{{ $Post->title }}" />
    @endif
    </a>
  </div>
  <div class="col-md-8">
    <h3 class="article-title"><a href="{{ route('category.article.show', [$Category->slug, $Post->slug]) }}">{{ $Post->title }}</a></h3>
    <p class="paragraph-indent">
    {{ str_limit(strip_tags($Post->contents), 400) }}
    <br>
    </p>
    <p><a href="{{ route('category.article.show', [$Category->slug, $Post->slug]) }}" class="btn btn-default">Baca Selanjutnya</a></p>
  </div>
</div>
@endforeach
@if(count($Posts) == 0)
<div class="row Post text-center">
  <b>Belum ada artikel</b>
</div>
@else
<div class="text-center">
  {!! $Posts->render() !!}
</div>
@endif
@endsection
