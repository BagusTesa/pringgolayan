@extends('user.layouts.master')
@section('title') {{ $Catalog->name }} - {{ $Product->name }} @endsection
@section('meta')
  <meta name="keyword"     content="Pringgolayan,Banguntapan,Kerajinan Kuningan,Kota Gede,Produk,{{ $Product->name }}">
  <meta name="description" content="{{ str_limit(strip_tags($Product->description), 140) }}">
  <?php
    $fURL         = route('catalog.product.show', [$Catalog->slug, $Product->slug]);
    $fType        = 'product.item';
    $fTitle       = $Catalog->name . ' - ' . $Product->name;
    $fDescription = $Product->description;
    if($Product->Image->first() != null)
    {
      $fImage     = $Product->Image->first()->image_path;
    }
    else
    {
      $fImage     = null;
    }
  ?>
  @include('user.layouts.facebook-meta')
@endsection
@section('css')
  <link href="{{ asset('assets/css/katalog.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/produk.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/fb-responsive.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/sharer.css') }}" rel="stylesheet">
@endsection
@section('js')
  <script src="{{ asset('assets/js/tidify.js') }}"></script>
  @include('user.layouts.sharejs')
@endsection
@section('contents')
<h2>{{ $Product->name }}</h2>
<hr>
<div class="row ">
  <div class="col-sm-3">
    @if($Product->Image->first() != null)
    <img class="img-responsive img-thumbnail post-img" src="{{ route('images', $Product->Image->first()->image_path) }}" alt="{{ $Product->Image->first()->description }}" />
    @else
    <img class="img-responsive img-thumbnail post-img" src="{{ route('images', 'noImage.png') }}" alt="" />
    @endif
  </div>
  <div class="col-sm-9">
    <div class="row overflowable">
      <div class="col-md-12">
        <div class="row">
          <div class="col-xs-2">
            <strong>Nama</strong>
          </div>
          <div class="col-xs-1">
            <strong>:</strong>
          </div>
          <div class="col-xs-9">
            {{ $Product->name }}
          </div>
        </div>
        <div class="row">
          <div class="col-xs-2">
            <strong>Kategori</strong>
          </div>
          <div class="col-xs-1">
            <strong>:</strong>
          </div>
          <div class="col-xs-9">
            {{ $Catalog->name }}
          </div>
        </div>
        <div class="row hidden-xs">
          <div class="col-xs-2">
            <strong>Pembuat</strong>
          </div>
          <div class="col-xs-1">
            <strong>:</strong>
          </div>
        </div>
        <br>
        <div class="row hidden-xs">
          <div class="col-md-12">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              @foreach($Product->Producer as $Producer)
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading{{ $Producer->slug }}">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $Producer->slug }}" aria-expanded="true" aria-controls="collapse{{ $Producer->slug }}">
                      {{ $Producer->name }}
                    </a>
                  </h4>
                </div>
                <div id="collapse{{ $Producer->slug }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $Producer->slug }}">
                  <div class="panel-body">
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <colgroup>
                          <col span="1" style="width: 30%;"></col>
                          <col span="1" style="width: 70%;"></col>
                        </colgroup>
                        <tbody>
                          <tr>
                            <td>
                              <b>Alamat</b>
                            </td>
                            <td>
                              {{ $Producer->address }}
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <b>Kontak</b>
                            </td>
                            <td>
                              {{ $Producer->contact }}
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <div class="text-right">
                        <a href="{{ route('producer.show', $Producer->id) }}" class="btn btn-primary">Lihat Profil</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
        <div class="row visible-xs">
          <a href="#" data-toggle="modal" data-target="#DaftarProdusen" class="btn btn-primary">Daftar Produsen</a>
        </div>
      </div>
    </div>
  </div>
</div>
@if(strlen($Product->description) > 0)
<hr>
<div class="row content">
  <div class="col-md-12 text-justify paragraph-indent" id="contents">
    {!! $Product->description !!}
  </div>
</div>
@endif
<hr>
<?php
   $Images = $Product->Image;
   $count  = count($Images);
?>
@if($count > 0)
<h4>Gambar Terkait:</h4>
@for($i = 0; $i < $count; $i++)
<div class="row">
  @for($j = $i; ($j < ($i + 4)) && ($j < $count); $j++)
  <div class="col-md-3">
    <div class="thumbnail-container">
      <div class="thumbnail">
        <img class="thumbnail-img img-responsive" src="{{ route('images', $Images[$j]->image_path) }}" alt="{{ $Images[$j]->description }}" />
      </div>
    </div>
  </div>
  @endfor
  <?php $i = $j ?>
</div>
@endfor
<hr>
@endif
<div class="row">
  <div class="col-md-12">
    <div class="btn btn-group">
      <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('catalog.product.show', [$Catalog->slug, $Product->slug]) }}" title="Bagikan di Facebook" target="_blank" class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
      <a href="http://twitter.com/home?status={{ route('catalog.product.show', [$Catalog->slug, $Product->slug]) }}" title="Bagikan di Twitter" target="_blank" class="btn btn-twitter"><i class="fa fa-twitter"></i> Twitter</a>
      <a href="https://plus.google.com/share?url={{ route('catalog.product.show', [$Catalog->slug, $Product->slug]) }}" title="Bagikan pada Google+" target="_blank" class="btn btn-googleplus"><i class="fa fa-google-plus"></i> Google+</a>
      <a href="#" data-toggle="modal" data-target="#share" class="btn btn-usb"><i class="fa fa-share"></i> Bagikan</a>
    </div>
  </div>
</div>
<div class="row center-block">
  <!-- Facebook comment plugin -->
  <div class="fb-comments" data-href="{{ route('catalog.product.show', [$Catalog->slug, $Product->slug]) }}" data-numposts="5" data-colorscheme="light"></div>
</div>
<div class="modal fade" id="share" tabindex="-1" role="dialog" aria-labelledby="shareLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="shareLabel">Share Link</h4>
      </div>
      <div class="modal-body">
        <textarea class="form-control" style="resize:none;" onclick="this.focus();this.select()" readonly>Pringgolayan | {{ $Catalog->name }} - {{ $Product->name }}: {{ route('catalog.product.show', [$Catalog->slug, $Product->slug]) }}</textarea>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="DaftarProdusen" tabindex="-1" role="dialog" aria-labelledby="DaftarProdusenLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="DaftarProdusenLabel">Daftar Produsen</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
           <div class="table-responsive">
             <table class="table table-hover">
               <tr>
                 <th>Nama</th>
                 <th>Alamat</th>
               </tr>
               @foreach($Product->Producer as $Producer)
               <tr>
                 <td><a href="{{ route('producer.show', $Producer->slug) }}">{{ $Producer->name }}</a></td>
                 <td>{{ $Producer->address }}</td>
               </tr>
               @endforeach
             </table>
           </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
