@extends('user.layouts.master')
@section('title') Profil {{ $Producer->name }} @endsection
@section('meta')
  <meta name="keywords" content="Pringgolayan,Banguntapan,Kerajinan Kuningan,Kota Gede,Artikel,{{ $Producer->name }}">
  <meta name="description" content="{{ 'Warga Pringgolayan ' . $Producer->name . ' Banguntapan, Yogyakarta' }}">
  <?php
    $fURL         = route('producer.show', $Producer->slug);
    $fType        = 'article';
    $fTitle       = 'Profil - ' . $Producer->name;
    $fDescription = 'Warga Pringgolayan ' . $Producer->name . ' Banguntapan, Yogyakarta';
    if($Producer->Image != null)
    {
      $fImage     = $Producer->Image->image_path;
    }
    else
    {
      $fImage     = null;
    }
  ?>
  @include('user.layouts.facebook-meta')
@endsection
@section('css')
  <link href="{{ asset('assets/css/katalog.css') }}" rel="stylesheet">
@endsection
@section('js')
@endsection
@section('contents')
<div class="row">
  <div class="col-sm-4">
    @if($Producer->Image != null)
    <img src="{{ route('images', $Producer->Image->image_path) }}" alt="{{ $Producer->name }}" class="img-responsive img-thumbnail post-img"/>
    @else
    <img src="{{ route('images', 'noImageP.png') }}" alt="{{ $Producer->name }}" class="img-responsive img-thumbnail post-img"/>
    @endif
  </div>
  <div class="col-sm-8">
    <div class="row">
      <div class="col-xs-2">
        <strong>Nama</strong>
      </div>
      <div class="col-xs-1">
        :
      </div>
      <div class="col-xs-9">
        {{ $Producer->name }}
      </div>
    </div>
    <div class="row">
      <div class="col-xs-2">
        <strong>Alamat</strong>
      </div>
      <div class="col-xs-1">
        :
      </div>
      <div class="col-xs-9">
        {{ $Producer->address }}
      </div>
    </div>
    <div class="row">
      <div class="col-xs-2">
        <strong>Kontak</strong>
      </div>
      <div class="col-xs-1">
        :
      </div>
      <div class="col-xs-9">
        {{ $Producer->contact }}
      </div>
    </div>
  </div>
</div>
@if(strlen($Producer->description) > 0)
<hr>
<div class="row content">
  <div class="col-md-12 text-justify paragraph-indent" id="contents">
    {!! $Producer->description !!}
  </div>
</div>
@endif
<hr>
<div class="row">
  <div class="col-md-12">
    <h2>Produk</h2>
  </div>
</div>
<?php $u = count($Products); ?>
@for($i = 0; $i < $u; $i++)
<div class="row center-block">
  @for($p = $i; ($p < ($i+3)) && ($p < $u); $p++)
  <div class="col-sm-3 product-list">
    <div class="row">
      <div class="thumbnail-container">
        <div class="thumbnail">
          <a href="{{ route('catalog.product.show', [$Products[$p]->Catalog->slug, $Products[$p]->slug]) }}">
          @if($Products[$p]->Image->first() != null)
          <img src="{{ route('images', [$Products[$p]->Image->first()->image_path]) }}" class="thumbnail-img img-responsive" alt="{{ $Products[$p]->Image->first()->description }}" />
          @else
          <img src="{{ route('images', 'noImage.png') }}" class="thumbnail-img img-responsive" alt="{{ $Products[$p]->name }}" />
          @endif
          </a>
        </div>
      </div>
    </div>
    <div class="row text-center">
      <a href="{{ route('catalog.product.show', [$Products[$p]->Catalog->slug, $Products[$p]->slug]) }}">
      <strong>{{ $Products[$p]->name }}</strong>
      </a>
    </div>
    <div class="row text-center">
      {{ str_limit(strip_tags($Products[$p]->description), 45) }}
    </div>
  </div>
  @endfor
  <?php $i += 2; ?>
</div>
@endfor
@if(count($Products) == 0)
<div class="row Post text-center">
  <b>Belum ada produk</b>
</div>
@else
<div class="text-center">
  {!! $Products->render() !!}
</div>
@endif
@endsection
