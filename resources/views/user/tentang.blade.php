@extends('user.layouts.master')
@section('title') Profil Dusun @endsection
@section('meta')
  <meta name="keywords" content="Pringgolayan,Banguntapan,Kerajinan Kuningan,Kota Gede">
  <meta name="description" content="Tentang Dusun Pringgolyan, Banguntapan">
  <?php
    $fURL         = route('root');
    $fType        = 'article';
    $fTitle       = 'Profil Dusun';
    $fDescription = 'Tentang Dusun Pringgolayan, Banguntapan';
    if($Demographics->first() != null)
    {
      if($Demographics->first()->Image != null)
      {
        $fImage   = $Demographics->first()->Image->image_path;
      }
      else
      {
        $fImage = null;
      }
    }
    else
    {
      $fImage     = null;
    }
  ?>
  @include('user.layouts.facebook-meta')
@endsection
@section('css')
  <!-- Morris Charts CSS -->
  <link href="{{ asset('vendor/morrisjs/morris.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/tentang.css') }}" rel="stylesheet">
@endsection
@section('js')
  <!-- JSSor Slider -->
  <script src="{{ asset('vendor/jssor-slider/js/jssor.js') }}"></script>
  <script src="{{ asset('vendor/jssor-slider/js/jssor.slider.js') }}"></script>
  <script src="{{ asset('assets/js/tentang.js') }}"></script>
  <!-- Morris Charts JavaScript -->
  <script src="{{ asset('vendor/raphael/raphael-min.js') }}"></script>
  <script src="{{ asset('vendor/morrisjs/morris.min.js') }}"></script>
  <!-- Chart Data -->
  <script>
    new Morris.Donut({
    // ID of the element in which to draw the chart.
    element: 'chart-demografik',
    // Chart data records -- each entry in this array corresponds to a point on
    // the chart.
    data: [
      @foreach($Demographics as $Demographic)
      { label: '{{ "$Demographic->name" }}', value: {{ $Demographic->number }} },
      @endforeach
    ],
    resize: true
    });
  </script>
@endsection
@section('carousel')
  @include('user.layouts.carousel-tentang')
@endsection
@section('contents')
<h2>Tentang Pringgolayan</h2>
<div class="row">
  <div class="col-md-12 text-justify paragraph-indent">
    <p>
      Banguntapan memiliki kondisi yang masih cukup asri seperti di pedesaan tetapi letaknya berada di dekat Kota Yogyakarta. Secara umum, masyarakatnya tersebar cukup luas. Desa Banguntapan khususnya Dusun Pringgolayan mayoritas penduduknya adalah pengrajin tembaga dan kuningan. Pada zaman dulu, hampir semua warga adalah pengrajin tembaga dan kuningan, akan tetapi setelah terjadi krisis moneter jumlah pengrajin semakin berkurang dan kini hanya tersisa 50 pengrajin yang tergabung dalam kelompok “Pringgo Makmur”. Selain potensi ekonomi berupa kerajinan tembaga dan kuningan, Desa Banguntapan memiliki berbagai jenis potensi ekonomi lain seperti kerajinan batu akik, perak, patung, fiber glass, serta usaha lain seperti perikanan dan pengolahan emping.
    </p>
    <p>
      Desa Banguntapan tidak hanya menawarkan potensi ekonomi saja. Desa ini juga memiliki beberapa kelompok pemain jathilan dan gamelan. Potensi budaya ini dapat dimanfaatkan untuk membangun desa yang mandiri dan tetap menjaga warisan luhur. Potensi Desa Banguntapan ini tentu saja dapat didayagunakan sebagai daya tarik desa sebagai desa wisata.
    </p>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    @if(count($Authorities) > 0)
    <h3>Perangkat Dusun</h3>
    <hr>
    <!-- Perangkat Desa -->
    <?php $c = count($Authorities); ?>
    @for($i = 0; $i < $c; $i++)
    <div class="row">
      @for($j = $i; ($j < $c) && ($j < ($i + 2)); $j++)
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-12 text-center">
            @if($Authorities[$j]->Image != null)
            <img src="{{ route('images', $Authorities[$j]->Image->image_path) }}" alt="{{ $Authorities[$j]->Image->description }}" class="Authority-image" />
            @else
            <img src="{{ route('images', 'noImageP.png') }}" alt="{{ $Authorities[$j]->name }}" class="Authority-image"/>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            <strong>{{ $Authorities[$j]->name }}</strong><br>
            <i>{{ $Authorities[$j]->position }}</i>
          </div>
        </div>
      </div>
      @endfor
      <?php $i += 1; ?>
    </div>
    @endfor
    @endif
  </div>
  <div class="col-md-6">
    @if(count($Demographics) > 0)
    <h3>Data Demografi</h3>
    <hr>
    <!-- Chart Pengrajin -->
    <div class="" id="chart-demografik"></div>
    @endif
  </div>
</div>
@endsection
