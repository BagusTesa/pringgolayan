@extends('user.layouts.master')
@section('title') Pencarian - {{ $SearchTerm }} @endsection
@section('meta')
  <meta name="keywords" content="Pringgolayan,Banguntapan,Kerajinan Kuningan,Kota Gede,Artikel,{{ $SearchTerm }}">
  <meta name="description" content="{{ 'Hasil Pencarian ' . $SearchTerm . ' Website Dusun Pringgolayan, Banguntapan, Yogyakarta' }}">
  <?php
    $fURL         = route('search', $SearchTerm);
    $fType        = 'article';
    $fTitle       = 'Pencarian - ' . $SearchTerm;
    $fDescription = 'Hasil Pencarian ' . $SearchTerm . ' Website Dusun Pringgolayan, Banguntapan, Yogyakarta';
    $fImage       = null;
  ?>
  @include('user.layouts.facebook-meta')
@endsection
@section('css')
  <link href="{{ asset('assets/css/beranda.css') }}" rel="stylesheet">
@endsection
@section('js')
@endsection
@section('contents')

<!-- Flash Data -->
@include('common.layouts.form-alerter')
@include('common.layouts.form-success')

@if(count($ArticleResults) > 0)
@foreach($ArticleResults as $Article)
<div class="row Post">
  <div class="col-md-4">
    <a href="{{ route('category.article.show', [$Article->Category->slug, $Article->slug]) }}">
      @if($Article->intro_image != null)
      <img class="img-responsive center-block Post-image" src="{{ route('images', $Article->Image->image_path) }}" alt="{{ $Article->Image->description }}" />
      @else
      <img class="img-responsive center-block Post-image" src="{{ route('images', 'noImage.png') }}" alt="{{ $Article->title }}" />
      @endif
    </a>
  </div>
  <div class="col-md-8">
    <h3 class="article-title"><a href="{{ route('category.article.show', [$Article->Category->slug, $Article->slug]) }}">{{ $Article->title }}</a></h3> <small>Artikel</small>
    <p>
    {{ str_limit(strip_tags($Article->contents), 400) }}
    <br>
    </p>
    <p><a href="{{ route('category.article.show', [$Article->Category->slug, $Article->slug]) }}" class="btn btn-default">Baca Selanjutnya</a></p>
  </div>
</div>
@endforeach
@endif
@if(count($ProductResults) > 0)
@foreach($ProductResults as $Product)
<div class="row Post">
  <div class="col-md-4">
    <a href="{{ route('catalog.product.show', [$Product->Catalog->slug, $Product->slug]) }}">
      @if($Product->Image->first() != null)
      <img class="img-responsive center-block Post-image" src="{{ route('images', $Product->Image->first()->image_path) }}" alt="{{ $Product->Image->first()->description }}" />
      @else
      <img class="img-responsive center-block Post-image" src="{{ route('images', 'noImage.png') }}" alt="{{ $Product->name }}" />
      @endif
    </a>
  </div>
  <div class="col-md-8">
    <h3 class="article-title"><a href="{{ route('catalog.product.show', [$Product->Catalog->slug, $Product->slug]) }}">{{ $Product->name }}</a></h3> <small>Product</small>
    <p>
    {{ str_limit(strip_tags($Product->description), 400) }}
    <br>
    </p>
    <p><a href="{{ route('catalog.product.show', [$Product->Catalog->slug, $Product->slug]) }}" class="btn btn-default">Baca Selanjutnya</a></p>
  </div>
</div>
@endforeach
@endif

@endsection
