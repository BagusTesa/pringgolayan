@extends('user.layouts.master')
@section('title') Penduduk Pringgolayan @endsection
@section('meta')
  <meta name="keyword" content="Pringgolayan,Banguntapan,Kerajinan Kuningan,Kota Gede, Penduduk">
  <meta name="description" content="{{ 'Penududuk Dusun Pringgolyan, Banguntapan' }}">
  <?php
    $fURL         = route('producer.index');
    $fType        = 'article';
    $fTitle       = 'Penduduk Pringgolayan';
    $fDescription = 'Penududuk Dusun Pringgolyan, Banguntapan';
    if($Producers[0] != null)
    {
      if($Producers[0]->Image != null)
      {
        $fImage   = $Producers[0]->Image->image_path;
      }
      else
      {
        $fImage   = null;
      }
    }
    else
    {
      $fImage     = null;
    }
  ?>
  @include('user.layouts.facebook-meta')
@endsection
@section('css')
  <link href="{{ asset('assets/css/katalog.css') }}" rel="stylesheet">
@endsection
@section('js') @endsection
@section('contents')
<h2>Penduduk Pringgolayan</h2>
<?php $u = count($Producers); ?>
@for($i = 0; $i < $u; $i++)
<div class="row center-block">
  @for($p = $i; ($p < ($i+3)) && ($p < $u); $p++)
  <div class="col-md-3 product-list">
    <div class="row">
      <div class="thumbnail-container">
        <div class="thumbnail">
          <a href="{{ route('producer.show', [$Producers[$p]->slug]) }}">
          @if($Producers[$p]->Image != null)
          <img src="{{ route('images', [$Producers[$p]->Image->image_path]) }}" class="thumbnail-img img-responsive" alt="{{ $Producers[$p]->Image->description }}" />
          @else
          <img src="{{ route('images', 'noImageP.png') }}" class="thumbnail-img img-responsive" alt="{{ $Producers[$p]->name }}" />
          @endif
          </a>
        </div>
      </div>
    </div>
    <div class="row text-center">
      <a href="{{ route('producer.show', [$Producers[$p]->slug]) }}">
      <strong>{{ $Producers[$p]->name }}</strong>
      </a>
    </div>
    <div class="row text-center">
      {{ str_limit(strip_tags($Producers[$p]->address), 60) }}
    </div>
  </div>
  @endfor
  <?php $i += 2; ?>
</div>
@endfor
@if(count($Producers) == 0)
<div class="row Post text-center">
  <b>Belum ada data warga</b>
</div>
@else
<div class="text-center">
  {!! $Producers->render() !!}
</div>
@endif
@endsection
