@extends('user.layouts.master')
@section('title') Kategori @endsection
@section('meta')
  <meta name="keywords" content="Pringgolayan,Banguntapan,Kerajinan Kuningan,Kota Gede,Kategori Artikel">
  <meta name="description" content="{{ 'Kategori Web Dusun Pringgolyan, Banguntapan' }}">
  <?php
    $fURL         = route('category.index');
    $fType        = 'article';
    $fTitle       = 'Kategori';
    $fDescription = 'Kategori Web Dusun Pringgolyan, Banguntapan';
    $fImage       = null;
  ?>
  @include('user.layouts.facebook-meta')
@endsection
@section('css') @endsection
@section('js') @endsection
@section('carousel') @endsection
@section('contents')
<h2>Kategori</h2>
<hr>
<div class="row">
  <div class="col-md-12">
    @foreach($Categories as $Category)
    <div class="panel panel-default">
      <div class="panel-body">
        <h5><i class="fa fa-book"></i> <a href="{{ route('category.show', $Category->slug) }}">{{ $Category->name }}</a><br></h5>
      </div>
    </div>
    @endforeach
  </div>
</div>
<div class="text-center">
  {!! $Categories->render() !!}
</div>
@endsection
