@extends('user.layouts.master')
@section('title') {{ $Category->name }} - {{ $Post->title }} @endsection
@section('meta')
  <meta name="keywords"    content="{{ $Post->keywords }}">
  <meta name="description" content="{{ str_limit(strip_tags($Post->contents), 140) }}">
  <?php
    $fURL         = route('category.article.show', [$Category->slug, $Post->slug]);
    $fType        = 'article';
    $fTitle       = $Category->name . ' - ' . $Post->title;
    $fDescription = $Post->contents;
    if($Post->Image != null)
    {
      $fImage     = $Post->Image->image_path;
    }
    else
    {
      $fImage     = null;
    }
  ?>
  @include('user.layouts.facebook-meta')
@endsection
@section('css')
  <link href="{{ asset('assets/css/artikel.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/fb-responsive.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/sharer.css') }}" rel="stylesheet">
@endsection
@section('js')
  <script src="{{ asset('assets/js/tidify.js') }}"></script>
  @include('user.layouts.sharejs')
@endsection
@section('contents')
  <h2>{{ $Post->title }}</h2>
  <hr>
  @if($Post->Image != null)
  <img src="{{ route('images', $Post->Image->image_path) }}" alt="{{ $Post->Image->description }}" class="img-responsive center-block" />
  @endif
  <div id="contents" class="text-justify paragraph-indent">
    {!! $Post->contents !!}
  </div>
<div class="row">
  <div class="col-md-12">
    <div class="text-right">
      <small>Disunting terakhir pada: {{ $Post->updated_at->format('D, d-m-Y') }}</small>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="btn btn-group">
      <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('category.article.show', [$Category->slug, $Post->slug]) }}" title="Bagikan di Facebook" target="_blank" class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
      <a href="http://twitter.com/home?status={{ route('category.article.show', [$Category->slug, $Post->slug]) }}" title="Bagikan di Twitter" target="_blank" class="btn btn-twitter"><i class="fa fa-twitter"></i> Twitter</a>
      <a href="https://plus.google.com/share?url={{ route('category.article.show', [$Category->slug, $Post->slug]) }}" title="Bagikan pada Google+" target="_blank" class="btn btn-googleplus"><i class="fa fa-google-plus"></i> Google+</a>
      <a href="#" data-toggle="modal" data-target="#share" class="btn btn-usb"><i class="fa fa-share"></i> Bagikan</a>
    </div>
  </div>
</div>
<div class="row center-block">
  <!-- Facebook comment plugin -->
  <div class="fb-comments" data-href="{{ route('category.article.show', [$Category->slug, $Post->slug]) }}" data-numposts="5" data-colorscheme="light"></div>
</div>
<div class="modal fade" id="share" tabindex="-1" role="dialog" aria-labelledby="shareLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="shareLabel">Share Link</h4>
      </div>
      <div class="modal-body">
        <textarea class="form-control" style="resize:none;" onclick="this.focus();this.select()" readonly>Pringgolayan | {{ $Category->name }} - {{ $Post->title }}: {{ route('category.article.show', [$Category->slug, $Post->slug]) }}</textarea>
      </div>
    </div>
  </div>
</div>
@endsection
