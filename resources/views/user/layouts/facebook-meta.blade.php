<!-- Facebook Graph API -->
<meta property="og:sitename"      content="Website Dusun Pringgolayan" />
<meta property="og:locale"        content="id_ID" />
@if($fURL)
<meta property="og:url"           content="{{ $fURL }}" />
@endif
@if($fType)
<meta property="og:type"          content="{{ $fType }}" />
@endif
@if($fTitle)
<meta property="og:title"         content="{{ $fTitle }}" />
@endif
@if($fDescription)
<meta property="og:description"   content="{{ str_limit(strip_tags($fDescription), 140) }}" />
@endif
@if($fImage)
<meta property="og:image"         content="{{ route('images', $fImage) }}" />
@else
<meta property="og:image"         content="{{ asset('assets/img/logo2.png') }}" />
@endif
