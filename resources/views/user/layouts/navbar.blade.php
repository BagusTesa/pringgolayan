  <!-- Navigation Bar Begin -->
  <nav class="navbar navbar-default">
    <div class="container menubar">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-collapse" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ route('root') }}"><img class="logo img-responsive" src="{{ asset('assets/img/logo2.png') }}" alt="Pringgolayan" /></a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="menu-collapse">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="{{ route('root') }}">Beranda</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              Artikel Desa<span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <?php $CateCount = count($Categories); ?>
              @for($i = 0;($i < $CateCount) && ($i < 10); $i++)
                <li><a href="{{ route('category.show', $Categories[$i]->slug) }}">{{ $Categories[$i]->name }}</a></li>
              @endfor
              @if($CateCount > 10)
                <li class="text-center"><a href="{{ route('category.index') }}"><strong>Selanjutnya</strong></a></li>
              @endif
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              Produk Desa<span class="caret"></span></a>
            </a>
            <ul class="dropdown-menu">
              <?php $CataCount = count($Catalogs); ?>
              @for($i = 0;($i < $CataCount) && ($i < 10); $i++)
                <li><a href="{{ route('catalog.show', $Catalogs[$i]->slug) }}">{{ $Catalogs[$i]->name }}</a></li>
              @endfor
              @if($CataCount > 10)
                <li class="text-center"><a href="{{ route('catalog.index') }}"><strong>Selanjutnya</strong></a></li>
              @endif
            </ul>
          </li>
          <li><a href="{{ route('producer.index') }}">Profesi</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              Tentang Pringgolayan<span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li>
                <a href="{{ route('about.index') }}">Profil Dusun</a>
              </li>
              <li>
                <a href="{{ route('about.map') }}">Peta Dusun</a>
              </li>
            </ul>
          </li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
  <!-- Navigation Bar End -->
