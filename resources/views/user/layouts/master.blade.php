<!DOCTYPE html>
<html lang="id">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    @section('meta')

    @show
    <title> Pringgolayan | @yield('title') </title>
    <!-- Bootstrap -->
    <link href="{{ asset('vendor/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/common.css') }}" rel="stylesheet" >
    <link href="{{ asset('assets/img/favicon.png') }}" rel="shortcut icon">
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/sosmed.css') }}" rel="stylesheet" >
    @section('css')

    @show
  </head>
  <body>
    @include('user.layouts.navbar')
    @section('carousel')

    @show

    <!-- Content Begin -->
    <div class="container" id="body">
      <div class="row">
        <div class="col-md-9 col-sm-12">
        @section('contents')

        @show
        </div>
        @include('user.layouts.sidebar')
      </div>
    </div>

    @include('user.layouts.footer')

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    @section('js')

    @show
  </body>
</html>
