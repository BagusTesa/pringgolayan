<footer class="footer">
  <hr>
  <div class="container">
    <div class="row text-center">
      <div class="col-md-12">
        <small>
          Diberdaya oleh <strong>Pemuda Pringgolayan</strong>
          <br>
          Didesain oleh <strong>Tim KKN-PPM UGM 2015</strong>
        </small>
      </div>
    </div>
    <div class="row hidden-xs">
      <div class="col-sm-2">
        <div class="sponsor_container">
          <a href="http://www.pu.go.id/" class="sponsor"><img src="{{ asset('assets/img/Logo_Dinas-Pekerjaan-Umum.png') }}" alt="Dinas Pekerjaan Umum" class="center-block footer-sponsor"/></a>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="sponsor_container">
          <a href="http://waroengss.com/" class="sponsor"><img src="{{ asset('assets/img/logo_warung-ss.png') }}" alt="Warung SS" class="center-block footer-sponsor"/></a>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="sponsor_container">
          <a href="https://idwebhost.com/" class="sponsor"><img src="{{ asset('assets/img/logo_IDwebhost.png') }}" alt="IDwebhost" class="center-block footer-sponsor"/></a>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="sponsor_container">
          <a href="{{ route('root') }}" class="sponsor"><img src="{{ asset('assets/img/logo-kkn-ugm-2015.png') }}" alt="KKN-PPM UGM Banguntapan 2015" class="center-block footer-sponsor"/></a>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="sponsor_container">
          <a href="https://lppm.ugm.ac.id/info-kkn-ppm/" class="sponsor"><img src="{{ asset('assets/img/logo_KKN-PPM-UGM.jpg') }}" alt="KKN-PPM UGM" class="center-block footer-sponsor"/></a>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="sponsor_container">
          <a href="{{ route('root') }}" class="sponsor"><img src="{{ asset('assets/img/logo_pemuda.png') }}" alt="Pemuda Pemudi Pringgolayan" class="center-block footer-sponsor"/></a>
        </div>
      </div>
    </div>
  </div>
</footer>
