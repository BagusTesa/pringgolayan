<div class="col-md-3 hidden-sm hidden-xs">
  <h2>Navigasi</h2>
  <hr>
  <form action="{{ route('search') }}" method="post" class="form-horizontal">
    <legend>Pencarian</legend>
    <div class="input-group">
      <input class="form-control" type="text" name="term" placeholder="Search">
      <div class="input-group-btn">
        <button type="submit" class="btn btn-default">
          &nbsp<span class="glyphicon glyphicon-search"></span>
        </button>
      </div>
    </div>
    {!! csrf_field() !!}
  </form>
  <br>
  @if(count($LArticles) > 0)
  <legend>Artikel Terbaru</legend>
    @foreach($LArticles as $Article)
    <div class="row sidebar-post">
      <div class="col-md-12">
        <a href="{{ route('category.article.show', [$Article->Category->slug, $Article->slug]) }}">
          <strong>{{ Illuminate\Support\Str::words($Article->title, 3) }}</strong>
        </a>
        <br>
        {{ Illuminate\Support\Str::words(strip_tags($Article->contents), 9) }}
      </div>
    </div>
    @endforeach
  @endif
  @if(count($LProducts) > 0)
  <legend>Produk Terbaru</legend>
    @foreach($LProducts as $Product)
    <div class="row sidebar-post">
      <div class="col-md-4">
        <a href="{{ route('catalog.product.show', [$Product->Catalog->slug, $Product->slug]) }}">
          @if($Product->Image->first() != null)
          <img src="{{ route('images', $Product->Image->first()->image_path) }}" alt="{{ $Product->Image->first()->description }}" class="img-responsive center-block sidebar-post-image"/>
          @else
          <img src="{{ route('images', 'noImage.png') }}" alt="{{ $Product->title }}" class="img-responsive center-block sidebar-post-image"/>
          @endif
        </a>
      </div>
      <div class="col-md-8">
        <a href="{{ route('catalog.product.show', [$Product->Catalog->slug, $Product->slug]) }}">
          <strong>{{ Illuminate\Support\Str::words($Product->name, 3) }}</strong>
        </a>
        <br>
        {{ Illuminate\Support\Str::words(strip_tags($Product->description), 9) }}
      </div>
    </div>
    @endforeach
  @endif
  <legend>Media Sosial dan Kontak</legend>
  <div class="row text-center">
    <div class="col-md-12">
      <a href="{{ config('pringgolayan.social.youtube.url') }}" target="_blank" title="{{ config('pringgolayan.social.youtube.name') }}"><i class="fa fa-youtube fa-3x"></i></a>
      <a href="mailto:{{ config('pringgolayan.social.email.url') }}" target="_blank" title="{{ config('pringgolayan.social.email.name') }}"><i class="fa fa-envelope-o fa-3x"></i></a>
      <a href="{{ config('pringgolayan.social.facebook.url') }}" target="_blank" title="{{ config('pringgolayan.social.facebook.name') }}"><i class="fa fa-facebook-square fa-3x"></i></a>
      <a href="{{ config('pringgolayan.social.twitter.url') }}" target="_blank" title="{{ config('pringgolayan.social.twitter.name') }}"><i class="fa fa-twitter fa-3x"></i></a>
      <a href="{{ config('pringgolayan.social.instagram.url') }}" target="_blank" title="{{ config('pringgolayan.social.instagram.name') }}"><i class="fa fa-instagram fa-3x"></i></a>
      <a href="{{ config('pringgolayan.social.location.url') }}" target="_blank" title="{{ config('pringgolayan.social.location.name') }}"><i class="fa fa-globe fa-3x"></i></a>
    </div>
  </div>
  <br>
  <div class="row text-center">
    <div class="col-md-12">
      <i class="fa fa-phone fa-lg"></i> :  <strong>{{ config('pringgolayan.social.phone.number') }}</strong> ({{ config('pringgolayan.social.phone.name') }})
    </div>
  </div>
</div>
