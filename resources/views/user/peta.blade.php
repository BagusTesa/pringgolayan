@extends('user.layouts.master')
@section('title') Peta Dusun @endsection
@section('meta')
  <meta name="keywords" content="Pringgolayan,Banguntapan,Kerajinan Kuningan,Kota Gede,Peta">
  <meta name="description" content="Peta Dusun Pringgolayan, Banguntapan">
  <?php
    $fURL         = route('about.map');
    $fType        = 'article';
    $fTitle       = 'Peta Dusun';
    $fDescription = 'Peta Dusun Pringgolayan, Banguntapan';
    $fImage       = asset('assets/img/map.png');
  ?>
  @include('user.layouts.facebook-meta')
@endsection
@section('css')
  <link href="{{ asset('assets/css/tentang.css') }}" rel="stylesheet">
@endsection
@section('js') @endsection
@section('carousel') @endsection
@section('contents')
  <ul class="nav nav-tabs">
    <li class="active"><a href="#peta-dusun" data-toggle="tab">Peta Dusun</a></li>
    <li><a href="#peta-profesi" data-toggle="tab">Peta Persebaran Profesi</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane fade in active" id="peta-dusun">
      <img src="{{ asset('assets/img/map.png') }}" class="img-responsive" />
    </div>
    <div class="tab-pane fade" id="peta-profesi">
      <img src="{{ asset('assets/img/map-profesi.png') }}" class="img-responsive" />
    </div>
  </div>
@endsection
