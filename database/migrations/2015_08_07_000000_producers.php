<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Producers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producers', function(Blueprint $table){
          $table->increments('id');
          $table->string('name')->unique();
          $table->string('slug')->unique();
          $table->string('address');
          $table->string('contact')->nullable();
          $table->mediumText('description');
          $table->integer('image_id')->unsigned()->nullable();
          $table->timestamps();

          $table->foreign('image_id')->references('id')->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('producers');
    }
}
