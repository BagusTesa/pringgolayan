<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Authorities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authorities', function (Blueprint $table){
          $table->increments('id');
          $table->string('name');
          $table->string('position');
          $table->integer('order')->unsigned();
          $table->integer('image_id')->unsigned()->nullable();
          $table->timestamps();
          $table->softDeletes();

          $table->foreign('image_id')->references('id')->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('authorities');
    }
}
