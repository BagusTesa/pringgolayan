<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ArticleCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'article_categories';
        //manual seeds
        $name = 'Event';
        DB::table($table)->insert([
            'name'     => $name,
            'featured' => '1',
            'slug'     => str_slug($name)
          ]);
          $name = 'Kabar Dusun';
        DB::table($table)->insert([
            'name'     => $name,
            'featured' => '0',
            'slug'     => str_slug($name)
          ]);
        $name = 'KKN-PPM UGM 2015';
        DB::table($table)->insert([
            'name'     => $name,
            'featured' => '0',
            'slug'     => str_slug($name)
          ]);
    }
}
