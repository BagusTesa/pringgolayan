<?php

use Illuminate\Database\Seeder;

class DemographicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table  = 'demographics';

        $name   = 'Pengrajin Imitasi';
        $number = 10;
        DB::table($table)->insert([
          'name'   => $name,
          'number' => $number
          ]);

        $name   = 'Pengrajin Batu Akik';
        $number = 30;
        DB::table($table)->insert([
          'name'   => $name,
          'number' => $number
          ]);

        $name   = 'Konveksi';
        $number = 5;
        DB::table($table)->insert([
          'name'   => $name,
          'number' => $number
          ]);

        $name   = 'Pengrajin Patung';
        $number = 4;
        DB::table($table)->insert([
          'name'   => $name,
          'number' => $number
          ]);

        $name   = 'Perikanan';
        $number = 5;
        DB::table($table)->insert([
          'name'   => $name,
          'number' => $number
          ]);
    }
}
