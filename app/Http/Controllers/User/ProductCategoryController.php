<?php

namespace Pringgolayan\Http\Controllers\User;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Pringgolayan\Models\ProductCategories;
use Pringgolayan\Models\Products;
use Pringgolayan\Models\ProductImages;

class ProductCategoryController extends UserUIController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $Catalogs = ProductCategories::orderBy('name', 'asc')->paginate(10);
        $Catalogs->setPath(route('catalog.index'));
        $array = [
          'Catalogs' => $Catalogs
        ];
        $array = $this->BuildNavbar($array);
        return view('user.daftar-katalog', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($slug)
    {
        $Catalog = ProductCategories::where('slug', '=', $slug)->first();
        if($Catalog == null)
        {
          //no results via SEF, fall back to ID search
          $Catalog = ProductCategories::find($slug);
          if($Catalog == null)
          {
            //still no result.. throws 404
            return abort(404);
          }
        }
        $Products= $Catalog->Product();
        $Products= $Products->with('Image')->paginate(20);
        $Products->setPath(route('catalog.show', $slug));
        $array   = [
          'Catalog' => $Catalog,
          'Products'=> $Products
        ];
        $array   = $this->BuildNavbar($array);
        return view('user.katalog', $array);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
