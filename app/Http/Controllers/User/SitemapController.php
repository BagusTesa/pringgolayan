<?php

namespace Pringgolayan\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Cache;
use Carbon\Carbon;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Pringgolayan\Models\ArticleCategories;
use Pringgolayan\Models\Articles;
use Pringgolayan\Models\ProductCategories;
use Pringgolayan\Models\Products;
use Pringgolayan\Models\Authorities;
use Pringgolayan\Models\Demographics;
use Pringgolayan\Models\Producers;
use Pringgolayan\Models\Images;


use Pringgolayan\Sitemap\SitemapClassHelper;

/**
* Dummy Sitemap Generator (replace this with better implementation with traits or something, lol)
*/
class SitemapController extends Controller
{
    private $duration = 60;

    private function generate_sitemap($XMLstring)
    {
      return (new Response($XMLstring, '200'))->header('Content-Type', 'text/xml');
    }

    public function IndexXML()
    {
      if(!Cache::has('sitemap.root'))
      {
        $lastArticleTime = Articles::orderBy('updated_at', 'desc')->first();
        if($lastArticleTime)
        {
          $lastArticleTime = $lastArticleTime->updated_at;
        }
        $Category = new SitemapClassHelper(route('sitemap.category'), $lastArticleTime, 'monthly');

        $lastProductTime = Products::orderBy('updated_at', 'desc')->first();
        if($lastProductTime)
        {
          $lastProductTime = $lastProductTime->updated_at;
        }
        $Product = new SitemapClassHelper(route('sitemap.catalog'), $lastProductTime, 'monthly');

        $lastDemographicTime = Demographics::orderBy('updated_at', 'desc')->first();
        if($lastDemographicTime)
        {
          $lastDemographicTime = $lastDemographicTime->updated_at;
        }
        $About   = new SitemapClassHelper(route('sitemap.about'), $lastDemographicTime, 'yearly');

        $lastProducerTime = Producers::orderBy('updated_at', 'desc')->first();
        if($lastProducerTime)
        {
          $lastProducerTime = $lastProducerTime->updated_at;
        }
        $Producer = new SitemapClassHelper(route('sitemap.producer'), $lastProducerTime, 'monthly');

        $lastImageTime = Images::orderBy('updated_at', 'desc')->first();
        if($lastImageTime)
        {
          $lastImageTime = $lastImageTime->updated_at;
        }
        $Image = new SitemapClassHelper(route('sitemap.image'), $lastImageTime, 'monthly');

        $array = [
          $Category,
          $Product,
          $About,
          $Producer,
          $Image
        ];
        Cache::put('sitemap.root', $array, $this->duration);
      }
      else
      {
        $array = Cache::get('sitemap.root');
      }
      return $this->generate_sitemap(view('sitemap.sitemap-index', ['URIs' => $array])->render());
    }

    public function CategoryXML()
    {
      if(!Cache::has('sitemap.category'))
      {
        $Categories = ArticleCategories::all();
        $array = [];
        foreach($Categories as $Category)
        {
          $lastArticleTime = $Category->Article()->orderBy('updated_at', 'desc')->first();
          if($lastArticleTime)
          {
            $lastArticleTime = $lastArticleTime->updated_at;
          }
          $item = new SitemapClassHelper(route('category.show', $Category->slug), $lastArticleTime, 'monthly');
          array_push($array, $item);
        }
        $Articles  = Articles::with('Category')->get();
        foreach($Articles as $Article)
        {
          $item = new SitemapClassHelper(route('category.article.show', [$Article->Category->slug, $Article->slug]), $Article->updated_at, 'monthly');
          array_push($array, $item);
        }
        Cache::put('sitemap.category', $array, $this->duration);
      }
      else
      {
        $array = Cache::get('sitemap.category');
      }
      return $this->generate_sitemap(view('sitemap.sitemap', ['URIs' => $array])->render());
    }

    public function CatalogXML()
    {
      if(!Cache::has('sitemap.catalog'))
      {
        $Catalogs = ProductCategories::all();
        $array = [];
        foreach($Catalogs as $Catalog)
        {
          $lastProductTime = $Catalog->Product()->orderBy('updated_at', 'desc')->first();
          if($lastProductTime)
          {
            $lastProductTime = $lastProductTime->updated_at;
          }
          $item = new SitemapClassHelper(route('catalog.show', $Catalog->slug), $lastProductTime, 'monthly');
          array_push($array, $item);
        }
        $Products  = Products::with('Catalog')->get();
        foreach($Products as $Product)
        {
          $item = new SitemapClassHelper(route('catalog.product.show', [$Product->Catalog->slug, $Product->slug]), $Product->updated_at, 'monthly');
          array_push($array, $item);
        }
        Cache::put('sitemap.catalog', $array, $this->duration);
      }
      else
      {
        $array = Cache::get('sitemap.catalog');
      }
      return $this->generate_sitemap(view('sitemap.sitemap', ['URIs' => $array])->render());
    }

    public function AboutXML()
    {
      if(!Cache::has('sitemap.about'))
      {
        $array = [];
        $lastDemographicTime = Demographics::orderBy('updated_at', 'desc')->first();

        if($lastDemographicTime)
        {
          $lastDemographicTime = $lastDemographicTime->updated_at;
        }
        $item = new SitemapClassHelper(route('about.index'), $lastDemographicTime, 'yearly');
        array_push($array, $item);

        $item = new SitemapClassHelper(route('about.map'), (Carbon::createFromFormat('d-m-Y', '31-08-2015')), 'yearly');
        array_push($array, $item);

        //multiply by 2, as this page rarely update at all!
        Cache::put('sitemap.about', $array, $this->duration * 2);
      }
      else
      {
        $array = Cache::get('sitemap.about');
      }
      return $this->generate_sitemap(view('sitemap.sitemap', ['URIs' => $array])->render());
    }

    public function ProducerXML()
    {
      if(!Cache::has('sitemap.producer'))
      {
        $Producers = Producers::all();
        $array = [];
        foreach($Producers as $Producer)
        {
          $item = new SitemapClassHelper(route('producer.show', $Producer->slug), $Producer->updated_at, 'monthly');
          array_push($array, $item);
        }
        Cache::put('sitemap.producer', $array, $this->duration);
      }
      else
      {
        $array = Cache::get('sitemap.producer');
      }
      return $this->generate_sitemap(view('sitemap.sitemap', ['URIs' => $array])->render());
    }

    public function ImageXML()
    {
      if(!Cache::has('sitemap.images'))
      {
        $Images = Images::all();
        $array  = [];
        foreach($Images as $Image)
        {
          $item = new SitemapClassHelper(route('images', $Image->image_path), $Image->updated_at, 'monthly');
          array_push($array, $item);
        }
        Cache::put('sitemap.images', $array, $this->duration);
      }
      else
      {
        $array = Cache::get('sitemap.images');
      }
      return $this->generate_sitemap(view('sitemap.sitemap', ['URIs' => $array])->render());
    }
}
