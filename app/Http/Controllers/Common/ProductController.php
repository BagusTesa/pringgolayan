<?php

namespace Pringgolayan\Http\Controllers\Common;

use Illuminate\Http\Request;

use Storage;
use Cache;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Pringgolayan\Models\Products;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function jsonList(Request $request)
    {
      if($request->ajax())
      {
        $Products  = Products::all();
        $array     = [];
        foreach($Products as $Product)
        {
          $item = new \stdClass();
          $item->id   = $Product->id;
          $item->name = $Product->name;
          array_push($array, $item);
        }
        return response()->json($array);
      }
      else
      {
        //throw 403
        return abort(403);
      }
    }

}
