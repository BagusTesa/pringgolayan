<?php

namespace Pringgolayan\Http\Controllers\Auth;

use Pringgolayan\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

use Illuminate\Http\Request;
use Auth;

use Pringgolayan\Http\Controllers\Admin\AdminUIController as AdminUI;
use Pringgolayan\Http\Controllers\User\UserUIController as UserUI;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */
    protected $redirectTo = '/admin';
    use ResetsPasswords;

    protected $adminUI;
    protected $userUI;
    protected $subject;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct(AdminUI $adminUI, UserUI $userUI)
    {
      $this->adminUI = $adminUI;
      $this->userUI  = $userUI;
      $this->subject = 'Reset Password Admin Pringgolayan';
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEmail()
    {
      $array = [];
      $array = $this->adminUI->BuildNavbar($array);
      return view('auth.password', $array);
    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
     public function getReset($token = null)
    {
        if (is_null($token)) {
            return abort(403);
        }
        $array = [
          'token' => $token
        ];
        $array = $this->userUI->BuildNavbar($array);
        return view('auth.reset', $array);
    }
}
