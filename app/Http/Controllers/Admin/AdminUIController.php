<?php

namespace Pringgolayan\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Auth;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

class AdminUIController extends Controller
{
    public function BuildNavbar($inArray)
    {
      if(Auth::check())
      {
        $lUser      = Auth::user();
        $outArray = array_add($inArray, 'lUser', $lUser);
        return $outArray;
      }
      else
      {
        //if something fishy, throws 403
        return abort(403);
      }
    }
}
