<?php

namespace Pringgolayan\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Pringgolayan\Models\ArticleCategories;
use Pringgolayan\Models\Articles;

class ArticleCategoryController extends AdminUIController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $Categories = ArticleCategories::with('Article')->paginate(10);
        $Categories->setPath(route('admin.category.index'));
        $array      = [
          'Categories' => $Categories
        ];
        $array = $this->buildNavbar($array);
        return view('admin.daftar-kategori', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $array = [
          'isEdit' => false
        ];
        $array = $this->buildNavbar($array);
        return view('admin.kategori', $array);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      $name      = $request->input('name');
      $featured  = $request->input('featured');
      $slug      = str_slug($name);

      $nCategory = new ArticleCategories;
      $nCategory->name     = $name;
      $nCategory->featured = $featured;
      $nCategory->slug     = $slug;

      if($request->has('name') && $request->has('featured'))
      {
        $oCategory = ArticleCategories::where('slug', '=', $slug)->first();
        if($oCategory == null)
        {
          if($featured == 1)
          {
            $fCategory = ArticleCategories::where('featured', '=', 1)->get();
            //manually un-feature any other category
            foreach($fCategory as $fCat)
            {
              $fCategory->featured = 0;
              $fCategory->update();
            }
          }
          $nCategory->save();
          $request->session()->flash('success', 'Kategori <strong>' . $name . '</strong> telah disimpan');
          return redirect(route('admin.category.index'));
        }
        else
        {
          $request->session()->flash('alert', 'Kategori dengan nama SEF yang sama sudah ada');
        }
      }
      else
      {
        $request->session()->flash('alert', 'Formulir tidak lengkap, data belum dapat kami simpan');
      }

      $array =[
        'isEdit'   => true,
        'Category' => $nCategory,
      ];
      $array = $this->buildNavbar($array);
      return view('admin.kategori', $array);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $Posts = Articles::with('Image', 'Category', 'User')->where('category_id', '=', $id)->paginate(20);
        $Posts->setPath(route('admin.category.show', $id));
        $array = [
          'Posts' => $Posts
        ];
        $array = $this->buildNavbar($array);
        return view('admin.daftar-artikel', $array);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $Category = ArticleCategories::find($id);
        if($Category == null)
        {
          abort(404);
        }
        $array =[
          'isEdit'   => true,
          'Category' => $Category
        ];
        $array = $this->buildNavbar($array);
        return view('admin.kategori', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
      $name      = $request->input('name');
      $featured  = $request->input('featured');
      $slug      = str_slug($name);

      $nCategory = ArticleCategories::find($id);
      if($nCategory == null)
      {
        abort(404);
      }
      $nCategory->name     = $name;
      $nCategory->featured = $featured;
      $nCategory->slug     = $slug;

      if($request->has('name') && $request->has('featured'))
      {
        $oCategory = ArticleCategories::where('slug', '=', $slug)->first();
        if(($oCategory == null) || ($nCategory->id == $oCategory->id))
        {
          if($featured == 1)
          {
            $fCategory = ArticleCategories::where('featured', '=', 1)->get();
            //manually un-feature any other category
            foreach($fCategory as $fCat)
            {
              if($fCat->id != $nCategory->id)
              {
                $fCat->featured = 0;
                $fCat->update();
              }
            }
          }
          $nCategory->update();
          $request->session()->flash('success', 'Kategori <strong>' . $name . '</strong> telah diperbarui');
          return redirect(route('admin.category.index'));
        }
        else
        {
          $request->session()->flash('alert', 'Kategori dengan nama SEF yang sama sudah ada');
        }
      }
      else
      {
        $request->session()->flash('alert', 'Data yang diberikan tidak lengkap, kami belum dapat menyimpan Kategori anda');
      }
      $array =[
        'isEdit'   => true,
        'Category' => $nCategory,
      ];
      $array = $this->buildNavbar($array);
      return view('admin.kategori', $array);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, ArticleController $ArCInstance, Request $request)
    {
        $dCategory = ArticleCategories::find($id);
        if($dCategory != null)
        {
          foreach($dCategory->Article as $Article)
          {
            $ArCInstance->destroy($Article->id);
          }
          $dCategory->delete();
          $request->session()->flash('success', 'Kategori telah berhasil dihapus');
        }
        else
        {
          $request->session()->flash('alert', 'Terjadi kesalahan, kategori tidak dapat dihapus');
        }
        return redirect(route('admin.category.index'));
    }

    public function search(Request $request)
    {
        $array =[
          'SearchTerm'     => '-',
          'Categories' => null
        ];
        if($request->has('term'))
        {
          $SearchTerm             = $request->input('term');
          $ArticleCategoryResults = ArticleCategories::search($SearchTerm)->with('Article')->paginate(10);
          $ArticleCategoryResults->setPath(route('admin.category.search'));
          $ArticleCategoryResults->appends(['term' => $request->input('term')]);
          $array['SearchTerm']    = $SearchTerm;
          $array['Categories']    = $ArticleCategoryResults;
          $request->session()->flash('success', 'Ditemukan <strong>' . $ArticleCategoryResults->total() . ' kategori artikel</strong> dengan' .
                                      ' kata kunci <strong>' . $SearchTerm . '</strong>');
        }
        else
        {
          return redirect(route('admin.category.index'));
        }
        $array = $this->buildNavbar($array);
        return view('admin.daftar-kategori', $array);
    }

}
