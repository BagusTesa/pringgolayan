<?php

namespace Pringgolayan\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

use Pringgolayan\Models\Products;
use Pringgolayan\Models\Producers;
use Pringgolayan\Models\Images;

class ProducerController extends AdminUIController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $Producers = Producers::with('Product', 'Image')->paginate(10);
        $Producers->setPath(route('admin.producer.index'));
        $array = [
          'Producers' => $Producers,
        ];
        $array = $this->buildNavbar($array);
        return view('admin.daftar-produsen', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $array = [
          'isEdit'   => false,
          'Products' => Products::all(),
          'Images'   => Images::all()
        ];
        $array = $this->buildNavbar($array);
        return view('admin.produsen', $array);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $nProducer = new Producers;
        $nProducer->name        = $request->input('name');
        $nProducer->address     = $request->input('address');
        $nProducer->contact     = $request->input('contact');
        $nProducer->description = $request->input('description');
        if($request->input('image') != -1)
        {
          $nProducer->image_id = $request->input('image');
        }

        if($request->has('name'))
        {
          $slug = str_slug($nProducer->name);
          $oProducer = Producers::where('slug', '=', $slug)->first();
          if($oProducer == null)
          {
            $nProducer->slug = $slug;
            $nProducer->save();

            $request->session()->flash('success', 'Data produsen telah berhasil ditambahkan');
            return redirect(route('admin.producer.index'));
          }
          else
          {
            $request->session()->flash('alert', 'Data tidak dapat ditambahkan, nama sudah digunakan');
          }
        }
        else
        {
          $request->session()->flash('alert', 'Data tidak lengkap, produsen tidak dapat ditambahkan');
        }
        $array = [
          'isEdit'    => true,
          'Producer'  => $nProducer,
          'Images'    => Images::all(),
          'Products'  => Products::all()
        ];
        $array = $this->buildNavbar($array);
        return view('admin.produsen', $array);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      $Producer = Producers::find($id);
      if($Producer == null)
      {
        abort(404);
      }
      $array = [
        'isEdit'   => true,
        'Producer' => $Producer,
        'Products' => Products::all(),
        'Images'   => Images::all()
      ];
      $array = $this->buildNavbar($array);
      return view('admin.produsen', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $nProducer = Producers::find($id);
      if($nProducer == null)
      {
        abort(404);
      }

      if($nProducer != null)
      {
        $nProducer->name        = $request->input('name');
        $nProducer->address     = $request->input('address');
        $nProducer->contact     = $request->input('contact');
        $nProducer->description = $request->input('description');
        if($request->input('image') != -1)
        {
          $nProducer->image_id = $request->input('image');
        }
        $productsArr = [];
        if($request->has('product'))
        {
          $productsArr = $request->input('product');
        }

        if($request->has('name'))
        {
          $slug = str_slug($nProducer->name);
          $oProducer = Producers::where('slug', '=', $slug)->first();
          if(($oProducer == null) || ($oProducer->id == $nProducer->id))
          {
            //attach-ing product data (by detaching everything else, hope i don't exhaust ids quickly...)
            $nProducer->Product()->detach();
            foreach($productsArr as $product)
            {
              if($product != -1)
              {
                $nProducer->Product()->attach($product);
              }
            }

            $nProducer->slug = $slug;
            $nProducer->update();

            $request->session()->flash('success', 'Data <strong>' . $nProducer->name . '</strong> telah berhasil diperbarui');
            return redirect(route('admin.producer.index'));
          }
          else
          {
            $request->session()->flash('alert', 'Data tidak dapat diperbarui, nama sudah digunakan');
          }
        }
        else
        {
          $request->session()->flash('alert', 'Data tidak lengkap, produsen tidak dapat diperbarui');
        }
      }
      else
      {
        $request->session()->flash('alert', 'Terjadi kesalahan, gagal memperbarui data produsen');
        return redirect(route('admin.producer.index'));
      }
      $array = [
        'isEdit'    => true,
        'Producer'  => $nProducer,
        'Images'    => Images::all(),
        'Products'  => Products::all()
      ];
      $array = $this->buildNavbar($array);
      return view('admin.produsen', $array);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $dProducer = Producers::find($id);
        if($dProducer != null)
        {
          //sever relations
          $dProducer->Product()->detach();
          $dProducer->Image()->dissociate();

          $dProducer->delete();
          $request->session()->flash('success', 'Produsen telah berhasil dihapus');
        }
        else
        {
          $request->session()->flash('alert', 'Terjadi kesalahan, produsen tidak dapat dihapus');
        }
        return redirect(route('admin.producer.index'));
    }

    public function search(Request $request)
    {
      $array =[
        'SearchTerm' => '-',
        'Producers'  => null
      ];
      if($request->has('term'))
      {
        $SearchTerm                = $request->input('term');
        $ProducersResults          = Producers::search($SearchTerm)->with('Product', 'Image')->paginate(10);
        $ProducersResults->setPath(route('admin.producer.search'));
        $ProducersResults->appends(['term' => $request->input('term')]);
        $array['SearchTerm']       = $SearchTerm;
        $array['Producers']        = $ProducersResults;
        $request->session()->flash('success', 'Ditemukan <strong>' . $ProducersResults->total() . ' produsen </strong> dengan' .
                                    ' kata kunci <strong>' . $SearchTerm . '</strong>');
      }
      else
      {
        return redirect(route('admin.producer.index'));
      }
      $array = $this->buildNavbar($array);
      return view('admin.daftar-produsen', $array);
    }

}
