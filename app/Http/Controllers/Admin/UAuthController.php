<?php

namespace Pringgolayan\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

use Pringgolayan\Http\Requests;
use Pringgolayan\Http\Controllers\Controller;

class UAuthController extends AdminUIController
{
    public function login(Request $inStream)
    {
      $username = $inStream->input('username');
      $password = $inStream->input('password');

      if(Auth::attempt(['username' => $username, 'password' => $password]))
      {
        //do nothings, basically whether authenticated or not admin_route will decide what to do, lol
        Auth::user()->last_login = Carbon::Now();
        Auth::user()->save();
      }
      else
      {
        $inStream->session()->flash('alert', 'Maaf, password atau username anda salah');
      }
      return redirect(route('admin.root'));
    }

    public function logout(Request $request)
    {
      Auth::user()->last_logout = Carbon::Now();
      Auth::logout();
      $request->session()->flash('success', 'Anda sudah keluar dari <strong>Administrasi Web Pringgolayan</strong>');
      return redirect(route('admin.root'));
    }

    public function home()
    {
      if(Auth::check())
      {
        $array =[

        ];
        $array = $this->BuildNavbar($array);
        return view('admin.beranda', $array);
      }
      else
      {
        return view('admin.login');
      }
    }

}
