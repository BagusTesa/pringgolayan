<?php

namespace Pringgolayan\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Images extends Model
{
  use SearchableTrait;
  protected $tables    = 'images';
  protected $timestamp = true;

  protected $searchable = [
    'columns' => [
      'description' => 15,
      'name'        => 10,
      'image_path'  => 5
      ]
  ];

  public function Product()
  {
    return $this->belongsToMany('Pringgolayan\Models\Products', 'products_images', 'image_id', 'product_id')->withTimestamps();
  }

  public function Article()
  {
    return $this->hasMany('Pringgolayan\Models\Articles', 'intro_image', 'id');
  }

  public function Authority()
  {
    return $this->hasMany('Pringgolayan\Models\Authorities' , 'image_id', 'id');
  }

  public function Demography()
  {
    return $this->hasMany('Pringgolayan\Models\Demographics', 'image_id', 'id');
  }

  public function Producer()
  {
    return $this->hasMany('Pringgolayan\Models\Producers', 'image_id', 'id');
  }
}
