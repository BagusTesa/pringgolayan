<?php

namespace Pringgolayan\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Products extends Model
{
   use SearchableTrait;
   protected $tables    = 'products';
   protected $timestamp = true;

   protected $searchable=[
     'columns'   => [
         'name'  => 15
       ]
   ];

   public function Catalog()
   {
     return $this->belongsTo('Pringgolayan\Models\ProductCategories', 'catalog_id', 'id');
   }

   public function Image()
   {
     return $this->belongsToMany('Pringgolayan\Models\Images', 'products_images', 'product_id', 'image_id')->withTimestamps();
   }

   public function Producer()
   {
     return $this->belongsToMany('Pringgolayan\Models\Producers', 'producers_products', 'product_id', 'producer_id')->withTimestamps();
   }
}
