<?php
/**
*   Pringgolayan Configuration, to be user all over the code.. lol
*/
return [
  /**
  *  Social Media URL
  */
  'social'       => [
      'facebook' => [
        'url'    => '#',
        'name'   => '#'
      ],
      'twitter'  => [
        'url'    => 'http://twitter.com/pringgolayan_g',
        'name'   => '@pringgolayan_g'
      ],
      'youtube'  => [
        'url'    => 'http://www.youtube.com/channel/UCDLsPV1hJC4bI0MRCcKJVGQ',
        'name'   => 'Muda Mudi Pringgolayan'
      ],
      'instagram'=> [
        'url'    => 'http://instagram.com/pringgolayanyk',
        'name'   => '@pringgolayanYK'
      ],
      'email'    => [
        'url'    => 'pringgolayanyk@gmail.com',
        'name'   => 'pringgolayanyk@gmail.com'
      ],
      'location'    => [
        'url'    => 'http://www.google.co.id/maps/place/-7.824543, 110.406700',
        'name'   => 'Lokasi Pringgolayan'
      ],
      'phone'    => [
        'number' => '+62 800 0000 0000',
        'name'   => 'Fulana'
      ]
    ]
];
